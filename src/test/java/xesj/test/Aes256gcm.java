package xesj.test;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import xesj.tool.ByteTool;
import xesj.tool.StringTool;

/**
 * AES-256-GCM titkosítás password + salt módon.
 */
public class Aes256gcm {
  
  static final int RIGHT_PAD = 16;
  
  /**
   * Algoritmusok 
   */
  static final String SECRET_KEY_ALGORITHM = "AES";
  static final String CIPHER_ALGORITHM = "AES_256/GCM/NoPadding";

  /** 
   * Jelszavak 
   */
  static String[] passwordArray = {
    "x", 
    "*/4Rt@&reSQ-)", 
    "qfweuijhwui wheuique dhuiorefgui hwhueiofui whuiowuier hwheuiowui8937293 3892"
  };
  
  /** 
   * Salt-ok 
   */
  static String[] saltArray = {
    "00000000000000000000000000000000",
    "01020304050607080910111213141516",
    "e4b2769aCC5ae4410980dEFFe43198a3b23bb08372cafed86377119900115533ccaaffeeDD56"
  }; 
  

  /** Adatok */
  static String[] dataArray = {
    "rövid",
    StringTool.multiply("árvíztűrő TÜKÖRFÚRÓGÉPjdiewdo jewiod ejwéfo jewfioewj fioweéfj iowef eijoféj iosjf eiwofé jweiof jweiof johehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsujohehwfuewhfuilhwefuhsuflhsufiehwfiuhwefiuhewiufhidhuwhluhcludiuldshfuewlhfiuwehfiuwlhduéhdhdfufuudsu", 30)
  };

  
  /**
   * main()
   */
  public static void main(String[] args) throws Exception {
    
    // Nagyciklus
    for (String password: passwordArray) {
      for (String salt: saltArray) {
        for (String data: dataArray) {

          // Előkészítés
          long t1 = System.currentTimeMillis();
          byte[] dataBytes = data.getBytes(StandardCharsets.UTF_8);
          byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
          byte[] saltBytes = ByteTool.hexToByteArray(salt);
          print("", "");
          print("data length", String.valueOf(data.length()));
          print("password", password);
          print("salt", salt);
          print("salt length", String.valueOf(salt.length()));

          // SecretKey előállítása
          SecretKeySpec secretKey = generateSecretKey(passwordBytes, saltBytes);

          // GCM paraméter előállítása (mindig más)
          byte[] iv = new byte[16];
          new SecureRandom().nextBytes(iv);
          print("IV", iv);
          GCMParameterSpec gcmParameter = new GCMParameterSpec(128, iv);

          // Encryption
          Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
          cipher.init(Cipher.ENCRYPT_MODE, secretKey, gcmParameter);
          byte[] encryptBytes = cipher.doFinal(dataBytes);

          // Decryption
          cipher = Cipher.getInstance(CIPHER_ALGORITHM);
          cipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameter);
          byte[] data2Bytes = cipher.doFinal(encryptBytes);

          // Ellenőrzés
          String data2 = new String(data2Bytes, StandardCharsets.UTF_8);
          if (!data.equals(data2)) {
            throw new RuntimeException("Sikertelen művelet, nem egyeznek az adatok");
          }

          // Eltelt idő
          long t2 = System.currentTimeMillis();
          print("Time", (t2 - t1) + " ms.");

        }  
      }  
    } // End: nagyciklus
    
  }
  
  /**
   * Secret key előállítása
   */
  static SecretKeySpec generateSecretKey(byte[] passwordBytes, byte[] salt) throws NoSuchAlgorithmException {
    byte[] byteArray = sha256(passwordBytes);
    print("sha256", byteArray); // Kiírjuk, hogy milyen hash állt elő
    for (int i = 0; i < salt.length; i++) {
      byteArray[(i * 13) % 32] = salt[i];
      byteArray = sha256(byteArray);
      print("sha256", byteArray); // Kiírjuk, hogy milyen hash állt elő
    }
    print("secret key", byteArray); // Kiírjuk, hogy milyen hash állt elő
    return new SecretKeySpec(byteArray, SECRET_KEY_ALGORITHM);
    
  } 
  
  /**
   * SHA-256 hash készítése
   * @param data Adat, melyből a hash készül
   * @return SHA-256 hash
   */
  static byte[] sha256(byte[] data) throws NoSuchAlgorithmException {

    MessageDigest md = MessageDigest.getInstance("SHA-256");
    return md.digest(data);    

  }  

  /**
   * String kiírása
   */
  static void print(String title, String text) {
    
    System.out.print(StringTool.rightPad(title + ":", RIGHT_PAD)); 
    if (text != null) {
      System.out.print(text);
    }  
    System.out.print("\n");
    
  }

  /**
   * Byte tömb tartalmának kiírása hexadecimális formában
   */
  static void print(String title, byte[] byteArray) {
    
    System.out.print(StringTool.rightPad(title + ":", RIGHT_PAD)); 
    if (byteArray != null) {
      System.out.print(ByteTool.byteArrayToHex(byteArray));
    }  
    System.out.print("\n");
    
  }
  
  // ====  
}
