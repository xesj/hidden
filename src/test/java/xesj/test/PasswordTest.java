package xesj.test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.app.system.Run;
import xesj.app.util.exception.BadPasswordException;
import xesj.app.util.password.Password;

/**
 * Password teszt
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Run.class)
@ActiveProfiles({"host-private-xesj"})
public class PasswordTest {
  
  /**
   * Konstruktor teszt
  */
  @Test
  public void constructorTest() {

    // Rossz jelszó
    badPassword("", "TEXT"); 
    badPassword("5Z=962982789232", "TEXT");                   // Túl rövid
    badPassword("0000000000000000000000000000000", "HEXA");   // Túl rövid
    badPassword("000000000000000000000000000000000", "HEXA"); // Túl hosszú
    badPassword("abcdef0123456789ABCDEFa0b1c2D35G", "HEXA");  // Hibás hexa karakter a végén

    // Jó jelszó
    goodPassword("                ", "TEXT"); 
    goodPassword("00000000000000000000000000000000", "HEXA");
    goodPassword("abcdef0123456789ABCDEFa0b1c2D3E4", "HEXA");
    goodPassword(
      "a a({ad$asÁRVÍZTŰRŐ árvíztűrődfsahsdahjsfdaksdfghjak*=\"sgdha'ksdg!jkasdhjkasjhgdh%jbcxhjkbhjckdsbchjksdbcjdks", 
      "TEXT"
    );
    
  }  
  
  /**
   * Rossz jelszó művelet
   */
  private void badPassword(String password, String passwordType) {
    
    Password.Type type = Password.Type.valueOf(passwordType);
    assertThrows(BadPasswordException.class, () -> new Password(password, type));
    
  }

  /**
   * Jó jelszó művelet
   */
  private void goodPassword(String password, String passwordType) {
    
    Password.Type type = Password.Type.valueOf(passwordType);
    new Password(password, type);
    
  }

  
  // ====
}
