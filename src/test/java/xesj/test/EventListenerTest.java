package xesj.test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.app.system.MainEventListener;
import xesj.app.system.Run;
import xesj.app.util.exception.BadSaltException;
import xesj.tool.ByteTool;
import xesj.tool.StringTool;

/**
 * EventListener teszt
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Run.class)
@ActiveProfiles({"host-private-xesj"})
public class EventListenerTest {
  
  @Autowired MainEventListener event;
  
  /**
   * saltToBytes() teszt
   */
  @Test
  public void saltToBytesTest() {

    // Rossz salt
    badSalt("");                                 // Túl rövid
    badSalt(StringTool.multiply("0", 31));       // Túl rövid
    badSalt(StringTool.multiply("0", 513));      // Túl hoszú
    badSalt(StringTool.multiply("0", 33));       // Páratlan
    badSalt(StringTool.multiply("0", 89));       // Páratlan
    badSalt("0000000000000000000000000000000x"); // Hibás hexa karakter
    badSalt("y000000000000000000000000000000x"); // Hibás hexa karakter
    badSalt("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"); // Hibás hexa karakter

    // Jó salt
    goodSalt(StringTool.multiply("0", 32));
    goodSalt(StringTool.multiply("f", 512));
    goodSalt(StringTool.multiply("0123456789abcdef", 2));
    goodSalt(StringTool.multiply("0123456789ABCDEF", 3));
    goodSalt(StringTool.multiply("9C", 16));
    goodSalt(StringTool.multiply("e3", 256));
    
  }  
  
  /**
   * Rossz salt művelet
   */
  private void badSalt(String salt) {
    
    assertThrows(BadSaltException.class, () -> event.saltToBytes(salt));
    
  }
  
  /**
   * Jó salt művelet
   */
  private void goodSalt(String salt) {
    
    byte[] bytes = event.saltToBytes(salt);
    String hex = ByteTool.byteArrayToHex(bytes);
    assertTrue(hex.equalsIgnoreCase(salt));
    
  }
  
  // ====
}
