package xesj.test;
import java.time.format.DateTimeFormatter;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.app.system.Run;
import xesj.app.util.exception.BadFileNameException;
import xesj.app.util.file.HiddenFile;

/**
 * HiddenFile teszt
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Run.class)
@ActiveProfiles({"host-private-xesj"})
public class HiddenFileTest {
  
  /**
   * Konstruktor teszt
  */
  @Test
  public void ConstructorTest() {

    // Rossz fájlnév
    badFileName("");
    badFileName("abc");
    badFileName("rossz-2021-10-03T12-38-59Xhidden"); // Kiterjesztés előtt nem pont van
    badFileName("X-1999-12-31T23-59-59.hidden"); // Hibás karakter a baseName-ben
    badFileName("árvíztűrő-1999-12-31T23-59-59.hidden"); // Hibás karakter a baseName-ben
    badFileName("-1999-12-31T23-59-59.hidden"); // Túl rövid
    badFileName("abcdefghijklmnopqr-01234stuvwxyz56789-bxhsb--cdscds-vfv-e-ever-v2-2000-01-01T00-00-00.hidden"); // Túl hosszú
    badFileName("abc-1999-02-29T23-59-59.hidden"); // Hibás szökőév

    // Jó fájlnév
    goodFileName(
      "jo-1999-12-31T23-59-59.hidden", 
      "jo", 
      "1999-12-31T23:59:59"
    );
    goodFileName(
      "x-1999-12-31T23-59-59.hidden", 
      "x", 
      "1999-12-31T23:59:59"
    );
    goodFileName(
      "szokoev-1996-02-29T23-59-59.hidden", 
      "szokoev", 
      "1996-02-29T23:59:59"
    );
    goodFileName(
      "abcdefghijklmnopqr-01234stuvwxyz56789-bxhsb--cdscds-vfv-e-ever-v-2000-01-01T00-00-00.hidden", 
      "abcdefghijklmnopqr-01234stuvwxyz56789-bxhsb--cdscds-vfv-e-ever-v",
      "2000-01-01T00:00:00"
    );
    
  }  
  
  /**
   * Rossz fájlnév művelet
   */
  private void badFileName(String fileName) {
    
    assertThrows(BadFileNameException.class, () -> new HiddenFile(fileName, (Math.random() > 0.5)));
    
  }
  
  /**
   * Jó fájlnév művelet
   */
  private void goodFileName(String fileName, String baseName, String time) {
    
    HiddenFile hiddenFile = new HiddenFile(fileName, (Math.random() > 0.5));
    assertEquals(baseName, hiddenFile.getBaseName());
    assertEquals(time, hiddenFile.getLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME));
    
  }
  
  // ====
}
