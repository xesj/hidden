/*******************************************************************************************************************************
 *
 * Modul: Fájlok törlése oldalhoz tartozó javascript.
 *   
 * Használat: a html-oldalon csak hivatkozni kell erre a javascript modulra: 
 *
 *   <script type="module" src=".../file-delete.js"></script>
 *
 ******************************************************************************************************************************/

// Change event-handler elhelyezése az "all1" checkboxra
const all1Checkbox = document.getElementById('all1');
if (all1Checkbox !== null) {
  all1Checkbox.addEventListener('change', allHandler);
}  

// Change event-handler elhelyezése az "all2" checkboxra
const all2Checkbox = document.getElementById('all2');
if (all2Checkbox !== null) {
  all2Checkbox.addEventListener('change', allHandler);
}

/**
 * "all1", "all2" checkbox change handler
 */
function allHandler(event) {

  // A hozzátartozó összes checkbox beállítása
  let checkboxes = document.querySelectorAll('.hidden-' + event.target.id);
  for (let checkbox of checkboxes) {
    checkbox.checked = event.target.checked;
  }

}

/******************************************************************************************************************************/