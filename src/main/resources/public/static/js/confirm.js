/*******************************************************************************************************************************
 * 
 * Modul: Megkeresi azokat a html elemeket, melyek rendelkeznek a "data-confirm" html-attribútummal. 
 * Ezeknek kattintható elemeknek kell lenniük, például: 
 * 
 *   <a ... data-confirm="<b>Valóban</b> a főlapra akar menni?">Link a főlapra</a>
 *   <button ... data-confirm="Valóban submit-álni akarod az űrlapot?">Űrlap submit</button>
 *
 * Ezen html elemekre kattintva felugrik egy bootstrap modal ablak, melynek szövegét a html elem "data-confirm"
 * attribútuma határozza meg. A szöveg html-ként lesz értelmezve. A modal ablakból kilépni csak az "Igen" vagy "Nem" gomb
 * megnyomásával lehet. "Igen" gomb megnyomásakor végrehajtódik a html elem default funkciója.
 *   
 * Használat: a html-oldalon csak hivatkozni kell erre a javascript modulra: 
 *
 *     <script type="module" src=".../confirm.js"></script>
 *
 *   A modul a lefutásakor automatikusan felderíti a "data-confirm" attribútummal ellátott elemeket,
 *   és elhelyezi rajtuk a szükséges event listener-eket. 
 *
 ******************************************************************************************************************************/

// Import
import {XesjError} from './xesjslib/error.js';

// Confirm-modal css-selector
const modalCssSelector = '#confirmModal';

// Confirm-modal elem megkeresése, meglétének ellenőrzése
const modalElem = document.querySelector(modalCssSelector);
if (modalElem === null) {
  throw new XesjError(`Nem létezik confirm-modal ilyen css-selector-ral: "${modalCssSelector}"`);
}

// Confirm-modal body elemének megkeresése
const modalBodyElem = document.querySelector(modalCssSelector + ' .modal-body');

// Confirm-modal "igen" gombjának megkeresése + click handler
const modalIgenElem = document.querySelector(modalCssSelector + ' .modal-footer button:first-child');
modalIgenElem.addEventListener('click', igenHandler);

// Az aktuális html-elem, ami kiváltja a modal megjelenését
let activeElem;

// Azon elemek megkeresése, melyek rendelkeznek a "data-confirm" attribútummal. Az elemekre click listener helyezése.
let confirmElements = document.querySelectorAll('[data-confirm]');
for (let confirmElem of confirmElements) {
  confirmElem.addEventListener('click', (event) => clickHandler(event));
}

// Attribútum, az "igen" gomb megnyomásának jelzésére
const dci = 'data-confirm-igen';

/**
 * Click eseménykezelő függvény
 */
function clickHandler(event) {

  // Az aktuális html-elem letárolása 
  activeElem = event.target;

  // Ha már volt "igen" válasz a modal ablakban, akkor nincs teendő, a html-elem default működést végezhet 
  if (activeElem.hasAttribute(dci)) {
    activeElem.removeAttribute(dci);
    return;
  }

  // Html-elem default működésének letiltása
  event.preventDefault();

  // Modal body szövegének beállítása
  modalBodyElem.innerHTML = activeElem.dataset.confirm;

  // Modal megjelenítése
  let modal = new globalThis.bootstrap.Modal(modalElem, { backdrop: 'static', keyboard: false });
  modal.show();
  
}

/**
 * "Igen" gomb handler
 */
function igenHandler() {

  // "Igen" beállítása az aktív html-elemnél, a clickHandler() számára
  activeElem.setAttribute(dci, '');
  
  // Click esemény generálása, clickHandler() kiváltása
  activeElem.click();

}

/******************************************************************************************************************************/