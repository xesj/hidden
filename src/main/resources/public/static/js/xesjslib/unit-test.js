/*******************************************************************************************************************************
 *
 * Modul: unit tesztelést segítő assert-függvény gyűjtemény.
 *
 * Használat:
 *
 *   assertEqual(osszead(2, 3), 5);
 *
 *   assertThrow(
 *     () => dateConvert.stringToDate('1968.22.33'), 
 *     XesjDataError
 *   );
 *
 ******************************************************************************************************************************/

/**
 * AssertError osztály, melyet a unit teszt metódusai váltanak ki.
 */
export class AssertError extends Error {

  // Konstruktor
  constructor(message) {

    super(message);
    this.name = this.constructor.name;

  }

}

/**
 * Elvárja hogy a paramétere null legyen, különben AssertError-t vált ki.
 * @param {any} a 
 */
export function assertNull(a) {

  if (a !== null) {
    throw new AssertError(`A várt érték null, de a kapott érték: ${a}`);
  }

}

/**
 * Elvárja hogy a paramétere boolean típusú true legyen, különben AssertError-t vált ki.
 * @param {boolean} b 
 */
export function assertTrue(b) {

  if (b !== true) {
    throw new AssertError(`A várt érték true, de a kapott érték: ${b}`);
  }

}

/**
 * Elvárja hogy a paramétere boolean típusú false legyen, különben AssertError-t vált ki.
 * @param {boolean} b 
 */
export function assertFalse(b) {

  if (b !== false) {
    throw new AssertError(`A várt érték false, de a kapott érték: ${b}`);
  }

}

/**
 * Elvárja hogy a két paraméterének típusa és értéke is megegyezzen, különben AssertError-t vált ki.
 * Csak a következő adattípusok esetén használható: 
 *   string
 *   boolean
 *   number
 *   null
 *   undefined
 * Nem használható object (tömb) esetén!  
 * @param {any} a 
 * @param {any} b 
 */
export function assertEqual(a, b) {

  if (a !== b) {
    throw new AssertError(`A két adat nem egyezik!\n1. ${typeof(a)} érték: ${a}\n2. ${typeof(b)} érték: ${b}`);
  }

}

/**
 * A függvény-paraméterét lefuttatja, és elvárja, hogy a paraméterében megadott típusú error váltódjon ki.
 * Ha nem váltódik ki az adott típusú error, akkor egy AssertError-t vált ki.
 * @param {function} f A lefuttatandó függvény.
 * @param {Error} e A függvény által kiváltandó error típusa. 
 */
export function assertThrow(f, e) {

  let noError = false;
  try {
    f();
    noError = true;
  }
  catch (e2) {
    if (!(e2 instanceof e)) {
      throw new AssertError(`A függvény ${e.name} helyett ${e2.name}-t váltott ki!`);
    }
  }
  if (noError) {
    throw new AssertError(`A függvény nem váltott ki ${e.name}-t!`);
  }

}

/******************************************************************************************************************************/