/*******************************************************************************************************************************
 * 
 * Modul: Backend service-ek kezelése.
 * Függvények gyűjteménye, melyek a hívásokat megvalósítják. 
 * A modult más xesjslib modul használja, de a függvények export-tal vannak ellátva, hogy bárhol haszálhatók legyenek.
 *
 ******************************************************************************************************************************/

import {isObject} from './convert.js';
import {XesjProtocolError} from './error.js';

/**
 * Http service hívása GET vagy POST módon olyan service felé, mely HSDE protokollal szolgáltatja a választ.
 * Példa http-GET hívásra: 
 * 
 *   let url = 'https://my-server:50001/my-app/osszead?a=3&b=5';
 *   let result = await httpServiceCall(url);
 *
 * Példa http-POST hívásra, ahol a header és a body is be van állítva:
 * 
 *   let url = 'https://my-server:50001/my-app/osszead';
 *   let headers = {
 *     'Content-Type': 'application/json',
 *     'crypto': 'Lite',
 *     'currency': 'Coin'
 *   };
 *   let body = '{"a": 3, "b": 5}';   // Ezt csak string-ként lehet átadni!
 *   let result = await httpServiceCall(url, {method: 'post', headers: headers, body: body});
 * 
 * A result objektum elemei:
 * 
 *   result.status   ->  http status, például: 200
 *   result.headers  ->  http header-ek, melyet Map-ként kapunk meg
 *   result.json     ->  http body, melyet objektumként kapunk meg
 *
 * Példa a kapott result feldolgozására:
 * 
 *   let result = await httpServiceCall(...);
 *   console.log(`http-status -> ${result.status}`);                
 *   let map = result.headers;                                  
 *   for (let key of map.keys()) {
 *     console.log(`header -> ${key}: ${map.get(key)`);
 *   }    
 *   console.log('body -> ' + JSON.stringify(result.json, null, 2));
 * 
 * @param {string} url A meghívandó url.
 * @param {object} options Hívás opciók, melyek a javascript fetch() metódusnak lesznek átadva. Nem kötelező!
 * @throws {XesjProtocolError} Ha a kommunikáció nem felel meg a HSDE-protokollnak.
 */
export async function httpServiceCall(url, options) {
    
  // Előkészítés
  const ERROR_TEXT = `Protokoll hiba!\nA hívott url: ${url}\n`;

  // Szerver oldal hívása
  let response = await fetch(url, options);

  // Ellenőrzés: a http-response content-type "application/json" ?
  let contentType = response.headers.get('Content-Type');
  if (contentType !== 'application/json' && !contentType.startsWith('application/json;')) {
    throw new XesjProtocolError(
      ERROR_TEXT + 
      `Nem megfelelő a http-response!\nContent-Type: ${contentType}!`
    );
  }

  // A http-response tartalmának lekérdezése 
  let json = await response.json();

  // Ellenőrzés: a válasz json tartalmaz "data" kulcsot ?
  if (json.data === undefined) {
    throw new XesjProtocolError(
      ERROR_TEXT +
      `A http-response nem tartalmaz "data" kulcsot!`
    );
  }

  // Ellenőrzés: a válasz json "data" kulcsához tartozó érték null, vagy objektum ?
  if (!isObject(json.data)) {
    throw new XesjProtocolError(
      ERROR_TEXT +
      `A http-response "data" kulcsához tartozó érték nem null, és nem objektum!`
    );
  }

  // Ellenőrzés: a válasz json tartalmaz "error" kulcsot ?
  if (json.error === undefined) {
    throw new XesjProtocolError(
      ERROR_TEXT + 
      `A http-response nem tartalmaz "error" kulcsot!`
    );
  }

  // Ellenőrzés: a válasz json "error" kulcsához tartozó érték null, vagy objektum ?
  if (!isObject(json.error)) {
    throw new XesjProtocolError(
      ERROR_TEXT +
      `A http-response "error" kulcsához tartozó érték nem null, és nem objektum!`
    );
  }

  // Sikeres protokoll
  return {
    status: response.status, 
    headers: response.headers,
    json: json
  };

} // End: httpServiceCall() function

/******************************************************************************************************************************/