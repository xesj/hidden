/*******************************************************************************************************************************
 *
 * Modul: xesjslib fő eleme
 *
 ******************************************************************************************************************************/

// Import
import {isString} from './convert.js';
import {XesjDataError} from './error.js';

/**
 * Xesjslib verzió
 */ 
export const VERSION = '3.3';

/**
 * Xesjslib-bel kompatibilis bootstrap verzió
 */
export const BOOTSTRAP_COMPATIBLE = '5';

/**
 * Html escape végrehajtása.
 * @param {string} text Szöveg.
 * @return {string} Html escape-elt szöveg. Null paraméter esetén null.
 * @throws {XesjDataError} Ha a paraméter nem string. 
 */
export function htmlEscape(text) {
  
  // Ellenőrzés: a paraméter string típusú?
  if (!isString(text)) {
    throw new XesjDataError('A paraméter nem string típusú!');
  }
  
  // A paraméter null?
  if (text === null) {
    return null;
  }
  
  // Escape
  return text
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;');

};

/**
 * Error kiváltása aszinkron módon. 
 * Lekezeletlen error kiváltása úgy, hogy a hívó folyamat nem szakad meg.
 * @param {object} error A kiváltandó hiba.
 */
export async function asyncThrow(error) {
  
  throw error;
  
}

/******************************************************************************************************************************/