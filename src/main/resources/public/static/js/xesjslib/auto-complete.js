/*******************************************************************************************************************************
 * 
 * Modul: Input mezőbe gépelt szövegtöredékhez megjeleníti a lehetséges szövegek listáját. 
 * 
 * Az input mezőnek tartalmaznia kell a következő attribútumot:
 * 
 *   - "data-xesjslib-auto-complete-url"
 *     Backend url, mely a szövegtöredékhez visszaadja a lehetséges szövegek listáját. Kötelező megadni.
 *     Az url-nek tartalmaznia kell egy "ACTEXT" szöveget egy query paraméter értékként, 
 *     ennek a helyére fogja a javascript behelyettesíteni a mezőben található szövegtöredéket, a backend hívás előtt.
 *     
 * Példa: 
 *
 *   <input type="text" data-xesjslib-auto-complete-url="/myapp/telepulesek?maximum=10&szovegtoredek=ACTEXT"/>
 *   
 * A backend-del való kommunikáció HSDE-protokollal történik. 
 * A backend által szolgáltatott json válasz "data" struktúrájának ilyennek kell lennie: 
 *
 *   data: {
 *     text: [string, string, ...],
 *     total: boolean
 *   }
 *
 *   ahol a "text" tömbként tartalmazza lehetséges szövegek listáját (ha nincs ilyen, akkor üres tömb),
 *   a "total" pedig meghatározza, hogy az összes lehetséges szöveget visszaadta-e a backend.
 *
 * Működés:     
 * A mezőbe íráskor megvárja amíg 1 másodpercig nem változik a mező tartalma, ezután meghívja a backend-et, átadva neki 
 * a mező tartalmát. A backend-et nem hívja meg, ha az input mező üres. Ha a backend nem adja vissza az összes lehetséges 
 * szöveget (total: false), akkor a szövegek listája alatt egy "..." sor jelenik meg a képernyőn.  
 *  
 * A html-oldalon hivatkozni kell erre a javascript modulra: 
 *
 *    <script type="module" src=".../auto-complete.js"></script>
 *
 * A modul a lefutásakor automatikusan felderíti a "data-xesjslib-auto-complete-url" attribútummal ellátott mezőket,
 * és elvégzi a szükséges teendőket. 
 * 
 ******************************************************************************************************************************/

// Import
import {isArray, isBoolean} from './convert.js';
import {XesjError} from './error.js';
import {httpServiceCall} from './service.js';

// Menü megjelenés várakozási ideje milliszekundumban
const WAIT = 1000;

// Menü elem. Ha nincs a képernyőn, akkor az értéke null.
let menu = null;

// Az aktív inputhandler azonosítója. Ha nincs ilyen akkor null.
// Egy inputhandlernek csak akkor kell műveletet végezni, ha az ő azonosítója ezzel egyezik!
let activeId = null;

// Inicializálás: a "data-xesjslib-auto-complete-url" attribútummal ellátott elemekhez handler rendelése.
let elements = document.querySelectorAll('[data-xesjslib-auto-complete-url]');
for (let elem of elements) {
  elem.addEventListener('input', inputHandler);
  elem.addEventListener('blur', blurHandler);
}

/**
 * InputHandler (a mező tartalma megváltozott)
 */
async function inputHandler(event) {

  menuRemove();
  activeId = Math.random();
  setTimeout(inputHandlerLate, WAIT, event.currentTarget, activeId);
  
}

/**
 * Késleltetett inputHandler
 */
async function inputHandlerLate(field, id) {
  
  // Aktív id ellenőrzése
  if (id !== activeId) {
    return;
  }
  
  // Üres mező esetén nincs menü
  if (field.value === '') {
    return;
  }

  // Http-service hívása
  let url = field.getAttribute('data-xesjslib-auto-complete-url').replace('ACTEXT', encodeURIComponent(field.value));
  let response = await httpServiceCall(url);
  let status = response.status;
  let json = response.json;
  
  // Http-státusz ellenőrzés
  if (status < 200 || status > 299) {
    throw new XesjError(
      `A hívott url: ${url}\n` + 
      `A http-státusz: ${status}\n` + 
      `A http-response "error" objektuma:\n${JSON.stringify(json.error, null, '  ')}`
    );
  }

  // A kapott adat
  let data = json.data;
  
  // Ellenőrzés: a data.text tömb típusú ? 
  if (data.text === null || !isArray(data.text)) {
    throw new XesjError(`A hívott url: ${url}\nA json válaszban a "data.text" nem tömb típusú!`);
  }
  
  // Ellenőrzés: a data.total boolean típusú ? 
  if (data.total === null || !isBoolean(data.total)) {
    throw new XesjError(`A hívott url: ${url}\nA json válaszban a "data.total" nem boolean típusú!`);
  }

  // Ha a szerver nem ad adatot, akkor nincs menü
  if (data.text.length === 0) {
    return;
  }

  // A mező pozíciója 
  let fieldRect = field.getBoundingClientRect();
  let fieldLeft = fieldRect.left + window.pageXOffset;
  let fieldBottom = fieldRect.bottom + window.pageYOffset;

  // Menü beállítása  
  let tmpMenu = document.createElement('div');
  tmpMenu.style.position = 'absolute';
  tmpMenu.style.top = `${fieldBottom + 8}px`; 
  tmpMenu.style.left = `${fieldLeft}px`; 
  tmpMenu.style.zIndex = '8'; 
  tmpMenu.className = 'list-group border border-primary-subtle border-3 shadow-lg';
  
  // Linkek létrehozása, és hozzáadásuk a menühöz
  for (const text of data.text) {
    tmpMenu.append(menuLink(text));
  }
  
  // "..." hozzáadása a menühöz ha szükséges
  if (!data.total) {
    tmpMenu.append(menuLink(null));
  }  
  
  // Mousedown esemény hozzárendelése a menühöz
  tmpMenu.addEventListener('mousedown', function (linkEvent) {
    linkEvent.preventDefault();
    let textReturn = linkEvent.target.getAttribute('text-return'); 
    if (textReturn !== null) {
      field.value = textReturn;
    }
    menuRemove();
  });
  
  // Menü hozzáadása a dokumentumhoz (megjelenítés a képernyőn), ha ez a függvény az aktív
  if (id === activeId) {
    menuRemove();
    menu = tmpMenu;
    document.body.append(menu);
  }
  
}

/**
 * Menü link előállítása.
 * @param {string} text A link szövege. Null érték esetén "..." lesz a szöveg, és nem fog visszaírni a mezőbe.  
 */
function menuLink(text) {
  
  let link = document.createElement('a');
  link.href = '#';
  link.className = 'list-group-item list-group-item-action';
  link.textContent = (text === null ? '...' : text);
  if (text !== null) {
    link.setAttribute('text-return', text);
  }  
  return link;
  
}

/**
 * Menü eltávolítása a dokumentumból (törlés a képernyőről)
 */
function menuRemove() {
  
  if (menu !== null) {
    menu.remove();
    menu = null;
  }  

}

/**
 * BlurHandler (a cursor elhagyja a mezőt) 
 */
function blurHandler(event) {

  activeId = null;
  menuRemove();

}

/******************************************************************************************************************************/