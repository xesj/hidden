/*******************************************************************************************************************************
 *
 * Modul: XesjError és leszármazottainak gyűjtőhelye. 
 * 
 * Hierarchia:
 *
 *   Error 
 *   |
 *   +---- XesjError 
 *             |
 *             +---- XesjDataError
 *             +---- XesjLimitError
 *             +---- XesjProtocolError
 *
 ******************************************************************************************************************************/

/**
 * Általános hiba
 */
export class XesjError extends Error {

  // Konstruktor
  constructor(message) {
    
    super(message);
    this.name = this.constructor.name;
    
  }

}

/**
 * Adathiba
 */
export class XesjDataError extends XesjError {

  // Konstruktor
  constructor(message) {

    super(message);
    this.name = this.constructor.name;

  }

}

/**
 * Határ/limit túllépése hiba
 */
export class XesjLimitError extends XesjError {

  // Konstruktor
  constructor(message) {

    super(message);
    this.name = this.constructor.name;

  }

}

/**
 * Protokoll hiba
 */
export class XesjProtocolError extends XesjError {

  // Konstruktor
  constructor(message) {
    
    super(message);
    this.name = this.constructor.name;
    
  }

}

/******************************************************************************************************************************/