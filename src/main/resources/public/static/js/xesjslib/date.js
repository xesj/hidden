/*******************************************************************************************************************************
 *
 * Modul: dátumok (Date példányok) kezelése.
 *
 * A modul elemei:
 *   - DateConvert osztály
 *     - stringToDate()
 *     - dateToString()
 *
 * A modul használata: 
 *
 *   import {DateConvert} from '.../date.js';
 *
 *   let convert = new DateConvert('-MM/DD/YYYY:)');
 *   let date = convert.stringToDate('.07.22.1968.......');    // Mon Jul 22 1968 00:00:00
 *   let text = convert.dateToString(date);                    // -07/22/1968:)
 *
 ******************************************************************************************************************************/

import {isString, stringToInteger} from './convert.js';
import {XesjDataError} from './error.js';

/**
 * Dátum konverziót megvalósító osztály. 
 * Az év csak a 100-9999 tartományba eshet!
 */
export class DateConvert {
  
  /**
   * Dátum formátum
   */
  formatMask = null;
  
  /**
   * Kezdőpozíciók a dátum formátumban. Ha a dátum formátum nem tartalmazza az adott kulcsot, akkor az értéke null.
   */
  position = {
    YYYY: null,
    MM: null,
    DD: null,
    hh: null,
    mm: null,
    ss: null,
    ttt: null
  };

  /**
   * Konstruktor
   * @param {string} formatMask 
   *   Dátum formátum. Kötelező adat.
   *   A formátum maszk tetszőleges lehet, de a benne szereplő néhány szövegelem speciális jelentéssel bír:
   *   YYYY   ->   év
   *   MM     ->   hónap
   *   DD     ->   nap
   *   hh     ->   óra
   *   mm     ->   perc
   *   ss     ->   másodperc
   *   ttt    ->   ezredmásodperc
   */
  constructor(formatMask) {

    // Ellenőrzés: formatMask ki van töltve, és string?
    if (formatMask === null || formatMask === '' || !isString(formatMask)) {
      throw new XesjDataError('A "formatMask" paraméternek nem üres stringnek kell lennie!');
    }
    
    // Pozíciók meghatározása
    for (let f in this.position) {
      let p1 = formatMask.indexOf(f);
      let p2 = formatMask.lastIndexOf(f);
      if (p1 !== p2) {
        throw new XesjDataError(
          `Érvénytelen dátum formátum! A(z) "${f}" többször szerepel a formátumban: "${formatMask}"!`
        );
      }
      if (p1 >= 0) {
        this.position[f] = p1;
      }
    }
    
    // A formátum helyes
    this.formatMask = formatMask;

  }
  
  /**
   * Szöveg konvertálása dátumra. 
   * A konvertálás az osztály belső formátum maszkjával történik, mely a konstruktorban lett megadva.
   * Ha a formátum maszkban az "YYYY", "MM", "DD" közül egy vagy több nincs megadva, 
   * akkor a keletkező dátumban az aktuális mai dátum éve, hónapja, napja kerül be. 
   * Ha a formátum maszkban a "hh", "mm", "ss", "ttt" közül egy vagy több nincs megadva, 
   * akkor a keletkező dátumban az óra, perc, másodperc, ezredmásodperc 0 értékkel kerül be. 
   * @param {string} text A szöveg. Lehet null vagy üres string is. 
   * @return {Date} A konvertálással kapott dátum. Null vagy üres string esetén, a dátum objektum null lesz.
   * @throws {XesjDataError} Ha a szöveg konvertálása nem lehetséges adathiba miatt.
   */
  stringToDate(text) {
    
    // Ellenőrzés: a paraméter string?
    if (!isString(text)) {
      throw new XesjDataError('A "text" paraméter nem string!');
    }
    
    // Null vagy üres string?
    if (text === null || text === '') {
      return null;
    }
    
    // Ellenőrzés
    if (text.length < this.formatMask.length) {
      throw new XesjDataError('A szöveg rövidebb mint a formátum!');
    }
    
    // Default dátum
    let today = new Date();
    
    // Év meghatározása
    let year = (
      this.position.YYYY === null ? 
      String(today.getFullYear()) : 
      text.substring(this.position.YYYY, this.position.YYYY + 4)
    );
    year = stringToInteger(year);  
    if (year < 100 || year > 9999) {
      throw new XesjDataError(`Az év nem az 100 - 9999 tartományba esik: ${year}!`);
    }

    // Hónap meghatározása
    let month = (
      this.position.MM === null ? 
      String(today.getMonth() + 1) : 
      text.substring(this.position.MM, this.position.MM + 2)
      );
    month = stringToInteger(month);
    if (month < 1 || month > 12) {
      throw new XesjDataError(`A hónap nem az 1 - 12 tartományba esik: ${month}!`);
    }
    
    // Nap meghatározása
    let dayMax = new Date(year, month, 0).getDate(); 
    let day = (
      this.position.DD === null ? 
      String(today.getDate()) : 
      text.substring(this.position.DD, this.position.DD + 2)
      );
    day = stringToInteger(day);
    if (day < 1 || day > dayMax) {
      throw new XesjDataError(`A nap nem az 1 - ${dayMax} tartományba esik: ${day}!`);
    }
 
    // Óra meghatározása
    let hour = (
      this.position.hh === null ? 
      '0' : 
      text.substring(this.position.hh, this.position.hh + 2)
      );
    hour = stringToInteger(hour);
    if (hour < 0 || hour > 23) {
      throw new XesjDataError(`Az óra nem a 0 - 23 tartományba esik: ${hour}!`);
    }
      
    // Perc meghatározása
    let minute = (
      this.position.mm === null ? 
      '0' : 
      text.substring(this.position.mm, this.position.mm + 2)
      );
    minute = stringToInteger(minute);
    if (minute < 0 || minute > 59) {
      throw new XesjDataError(`A perc nem a 0 - 59 tartományba esik: ${minute}!`);
    }
    
    // Másodperc meghatározása
    let second = (
      this.position.ss === null ? 
      '0' : 
      text.substring(this.position.ss, this.position.ss + 2)
      );
    second = stringToInteger(second);
    if (second < 0 || second > 59) {
      throw new XesjDataError(`A másodperc nem a 0 - 59 tartományba esik: ${second}!`);
    }
      
    // Ezredmásodperc meghatározása
    let thousandth = (
      this.position.ttt === null ? 
      '0' : 
      text.substring(this.position.ttt, this.position.ttt + 3)
      );
    thousandth = stringToInteger(thousandth);
    if (thousandth < 0 || thousandth > 999) {
      throw new XesjDataError(`Az ezredmásodperc nem a 0 - 999 tartományba esik: ${thousandth}!`);
    }
    
    // Dátum előállítása, és visszaadása
    return new Date(year, month - 1, day, hour, minute, second, thousandth);
    
  } // End: stringToDate()
  
  /**
   * Dátum konvertálása szövegre. 
   * A konvertálás az osztály belső formátum maszkjával történik, mely a konstruktorban lett megadva.
   * @param {Date} date A dátum. Lehet null is.
   * @return {string} A konvertálással kapott szöveg. Null paraméter esetén null.
   * @throws {XesjDataError} Ha a konvertálás nem lehetséges adathiba miatt.
   */
  dateToString(date) {
    
    // Van dátum?
    if (date === null) {
      return null;
    }
    
    // Paraméter típusának ellenőrzése
    if (!(date instanceof Date)) {
      throw new XesjDataError('A "date" paraméter nem Date példány!');
    }

    // Az év 100-9999 tartományba esik?
    let year = date.getFullYear();
    if (year < 100 || year > 9999) {
      throw new XesjDataError(`Az év nem az 100 - 9999 tartományba esik: ${year}!`);
    }
    
    // Válasz
    return this.formatMask
      .replace('YYYY', String(year).padStart(4, '0'))
      .replace('MM', String(date.getMonth() + 1).padStart(2, '0'))
      .replace('DD', String(date.getDate()).padStart(2, '0'))
      .replace('hh', String(date.getHours()).padStart(2, '0'))
      .replace('mm', String(date.getMinutes()).padStart(2, '0'))
      .replace('ss', String(date.getSeconds()).padStart(2, '0'))
      .replace('ttt', String(date.getMilliseconds()).padStart(3, '0'));
    
  } // End: dateToString()
  
} // End: DateConvert class

/******************************************************************************************************************************/