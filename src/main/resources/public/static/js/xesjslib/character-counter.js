/*******************************************************************************************************************************
 * 
 * Modul: Input mező karaktereinek számlálása, és megjelenítése.
 * Az input mezők a következők lehetnek:
 * 
 *   <input type="text">
 *   <input type="password">
 *   <textarea>
 * 
 * Az input mezőkhöz attribútumokat kell hozzárendelni:
 * 
 *   - data-xesjslib-character-counter-id="..."
 *     Html-id melynek a belsejébe íródik, hogy hol tart a gépelés.
 *     Kötelező attribútum!
 * 
 *   - data-xesjslib-character-counter-format="..."
 *     Számlálási információk megjelenésének formátuma.
 *     Használhatók benne a következők:
 *       {count}     = Hány karakter van a mezőben.
 *       {free}      = Hány szabad karakterhely van a mezőben.
 *       {maxlength} = Maximum hány karakter lehet a mezőben (ez a mező maxlength attribútuma).
 *       {percent}   = A mező telítettsége százalékban. Egész szám, mely nem tartalmaz százalékjelet!
 *     Példák:
 *       data-xesjslib-character-counter-format="{count}/{maxlength} karaktert már begépeltél, itt jársz: <b>{percent}%</b>"
 *       data-xesjslib-character-counter-format="Telítettség: {percent} százalék"
 *     Ha nincs megadva, akkor a default értéke: "{count}/{maxlength} karakter ({percent}%)"
 *     Tartalmazhat html tagokat is, mivel az adat innerHTML módon íródik az oldalra.
 * 
 * Teljes példa:
 * 
 *   <textarea maxlength="512" 
 *             data-xesjslib-character-counter-id="szamlalo" 
 *             data-xesjslib-character-counter-format="{count}/{maxlength}">
 *   </textarea>
 *   <div id="szamlalo"></div>
 * 
 * A modul használata: a html-oldalon hivatkozni kell erre a modulra: 
 * 
 *   <script type="module" src=".../character-counter.js"></script>
 * 
 * A modul a lefutásakor automatikusan felderíti a "data-xesjslib-character-counter-id" attribútummal ellátott html elemeket, 
 * és elvégzi a szükséges teendőket. 
 * 
 ******************************************************************************************************************************/

// Import
import {stringToInteger} from './convert.js';
import {XesjDataError} from './error.js';

// Default formátum
const DEFAULT_FORMAT = '{count}/{maxlength} karakter ({percent}%)'; 

// "data-xesjslib-character-counter-id" attribútummal rendelkező elemek keresése
let elements = document.querySelectorAll('[data-xesjslib-character-counter-id]');

// Ciklus az elemeken 
for (let elem of elements) {

  // Ellenőrzés: a "data-xesjslib-character-counter-id" értéke (html-id) létezik az oldalon?
  let id = elem.getAttribute('data-xesjslib-character-counter-id');
  if (document.getElementById(id) === null) {
    throw new XesjDataError('Ilyen html-id nem létezik: ' + id);
  }
  
  // Ellenőrzés: a "maxlength" attribútum létezik, és nemnegatív egész szám?
  let maxlength = stringToInteger(elem.getAttribute('maxlength'));
  if (maxlength === null) throw new XesjDataError('A "maxlength" attribútum nincs megadva!');
  if (maxlength < 0) throw new XesjDataError('A "maxlength" attribútum negatív: ' + maxlength);
  
  // Event listener beállítása
  elem.addEventListener('input', counterHandler);
  
  // Event listener futtatása
  elem.dispatchEvent(new Event('input'));
  
}

// counterHandler függvény
function counterHandler(event) {
  
  // Számítások
  let sourceElem = event.currentTarget;
  let count = sourceElem.value.length; 
  let maxlength = stringToInteger(sourceElem.getAttribute('maxlength'));
  let percent = Math.round(100 * count / maxlength);
  
  // Formátum meghatározása
  let format = sourceElem.getAttribute('data-xesjslib-character-counter-format');
  if (format === null || format === '') format = DEFAULT_FORMAT;
  
  // Kiírandó szöveg meghatározása
  let print = format
    .replaceAll('{count}', count)
    .replaceAll('{free}', maxlength - count)
    .replaceAll('{maxlength}', maxlength)
    .replaceAll('{percent}', percent);
    
  // Kiírás
  let id = sourceElem.getAttribute('data-xesjslib-character-counter-id');
  let destinationElem = document.getElementById(id);
  destinationElem.innerHTML = print;
  
}

/******************************************************************************************************************************/