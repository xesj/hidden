/*******************************************************************************************************************************
 *
 * Modul: html oldal ellenőrzése.
 * Ha az oldalon hibát talál, exception-t vált ki, mely megjelenik a konzolon.
 * 
 * Funkciók:
 *  
 *   1. Ellenőrzi, hogy a html oldalon egy html-id többször szerepel-e. 
 *      Ha egy elem id-je üres string az nem probléma, hiszen nincs adat, 
 *      pont olyan mintha az id attribútum nem is lenne megadva.  
 *
 * Használat:
 * 
 *   <script type="module" src=".../html-check.js"></script>
 * 
 * A modul a lefutásakor automatikusan felderíti a html hibákat, és kiírja őket a konzolra. 
 * A modul függvényei export-tal vannak ellátva azért, hogy a modul ne csak egyszer fusson le, 
 * hanem a html oldal módosítása után a modul használója újra meghívhassa az ellenőrző függvényeket.
 * 
 ******************************************************************************************************************************/

// Import
import {XesjDataError} from './error.js';
import {asyncThrow} from './main.js';

// Az összes ellenőrző funkció meghívása.
htmlCheck();

/**
 * Az összes ellenőrző függvényt meghívó függvény. 
 */ 
export function htmlCheck() {

  idCheck();
  
}

/**
 * Html-id ellenőrzés. 
 * Ha egy html-id többször szerepel az oldalon, az XesjDataError-t okoz, mely megjelenik a konzolon.
 * Ha egy elem id-je üres string az nem probléma, hiszen nincs adat, 
 * pont olyan mintha az id attribútum nem is lenne megadva.  
 */ 
export function idCheck() {
  
  // Előkészítés
  let map = new Map();
  
  // Id attribútummal ellátott elemek
  let elements = document.querySelectorAll('[id]');

  // Ciklus az elemeken 
  for (let elem of elements) {
    let id = elem.getAttribute('id');
    if (id !== null && id !== '') {
      let value = map.get(id);
      if (value === undefined) value = '0';
      map.set(id, Number(value) + 1);
    }  
  }

  // Map tartalom elemzése, XesjDataError kiváltása
  for (let key of map.keys()) {
    let value = map.get(key);
    if (value > 1) {
      asyncThrow(new XesjDataError(`A(z) "${key}" html-id ${value} elemnél fordul elő az oldalon!`));
    }  
  }  
  
}

/******************************************************************************************************************************/