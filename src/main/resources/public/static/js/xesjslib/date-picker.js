/*******************************************************************************************************************************
 * 
 * Modul: dátum választó megjelenítése.
 * 
 * A html oldalakon az input mezőkbe írandó dátumok kiválasztását segíti úgy, hogy az input mezőhöz egy dátum választót 
 * jelenít meg a képernyőn. A dátum választó a mai napot vastag betűvel jeleníti meg, az input mezőben lévő napot pedig 
 * világoskék háttérszínnel. Ha az input mező üres, vagy olyan szöveget tartalmaz amely nem felel meg a formátum maszknak, 
 * akkor a dátum választó az aktuális hónapot fogja megjeleníteni. A dátum választó csak a 100-9999 tartományban lévő
 * éveket jeleníti meg.
 *
 * A dátum választó használatához az input mezőt attribútumokkal kell ellátni. Ezek a következők:
 *
 *   - data-xesjslib-date-picker-activate="..."  
 *     Kötelező megadni! Egy másik html elem id-je, melyre "click" event handler kerül, így a másik elemre kattintva
 *     megjeleníti az input mező alatt a hozzátartozó dátum választót. Az input mező saját id-je is megadható,
 *     ekkor az input mezőre kattintva jelenik meg a dátum választó.
 *
 *   - data-xesjslib-date-picker-language="..."
 *     Nem kötelező megadni. Csak "hu" (magyar) vagy "en" (angol) lehet. Ha nincs megadva, akkor a default: "hu"
 *     Azt szabályozza, hogy milyen nyelven jelenjenek meg a dátum választóban a hónapok, és a hét napjai. 
 *     Ezeket a nemzetközi szövegeket a date-picker.js tartalmazza, nem lehet kívülről más szövegeket beállítani. 
 *
 *   - data-xesjslib-date-picker-format="..."
 *     Nem kötelező megadni. Az input mező dátum formátumát írja le. Ha nincs megadva, akkor a default: "YYYY.MM.DD".
 *     A formátum maszk speciális karakterei a date.js modulban a DateConvert osztály konstruktor leírásában található.
 *   
 * Példa a html oldalon lévő input mezőre, és a dátum választó aktiváló gombra:
 *
 *   <input type="text" data-xesjslib-date-picker-activate="b48"/>
 *   <button id="b48" type="button">dátum választás</button>
 *
 * A modul használatához html oldalon hivatkozni kell erre a modulra: 
 *
 *     <script type="module" src=".../date-picker.js"></script>
 *
 * A modul a lefutásakor automatikusan felderíti a "data-xesjslib-date-picker-activate" attribútummal ellátott mezőket,
 * és elvégzi a szükséges teendőket. 
 *   
 ******************************************************************************************************************************/

// Import
import {XesjDataError} from './error.js';
import {DateConvert} from './date.js';

/**
 * Dátum választó template
 */
const PICKER_TEMPLATE =
  `
  <table id="xesjslib-date-picker-id" 
         tabindex="-1" 
         class="xesjslib-date-picker table table-bordered table-sm border border-primary-subtle border-3 shadow-lg">
    <thead>
      <tr>
        <th class="active text-primary" data-shift-month="-1">&#10094;</th>
        <th class="passive text-primary" colspan="5"></th>
        <th class="active text-primary" data-shift-month="1">&#10095;</th>
      </tr>
      <tr>
        <th class="passive"></th>
        <th class="passive"></th>
        <th class="passive"></th>
        <th class="passive"></th>
        <th class="passive"></th>
        <th class="passive text-danger"></th>
        <th class="passive text-danger"></th>
      </tr>
    </thead>
    <tbody>
      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
    </tbody>
  </table>
  `;    

/**
 * Aktiválás html attribútum
 */
const ACTIVATE_ATTRIBUTE = 'data-xesjslib-date-picker-activate';

/**
 * Language html attribútum
 */
const LANGUAGE_ATTRIBUTE = 'data-xesjslib-date-picker-language';

/**
 * Formátum html attribútum
 */
const FORMAT_ATTRIBUTE = 'data-xesjslib-date-picker-format';

/**
 * Ha nincs megadva a "data-xesjslib-date-picker-language" html attribútum a mezőhöz, akkor ez a default beállítás.
 */
const DEFAULT_LANGUAGE = 'hu';

/**
 * Ha nincs megadva a "data-xesjslib-date-picker-format" html attribútum a mezőhöz, akkor ez a default beállítás.
 */
const DEFAULT_FORMAT = 'YYYY.MM.DD';

/**
 * Hónap, és nap feliratai különböző nyelveken.
 */
const TITLE = {
  hu: {
    month: ['január', 'február', 'március', 'április', 'május', 'június', 
            'július', 'augusztus', 'szeptember', 'október', 'november', 'december'],
    day: ['H', 'K', 'Sz', 'Cs', 'P', 'Sz', 'V']
  },
  en: {
    month: ['january', 'february', 'march', 'april', 'may', 'june', 
            'july', 'august', 'september', 'october', 'november', 'december'],
    day: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']      
  }
};

/**
 * Dátum választó. Ha nincs ilyen, akkor null.
 */
let picker = null;

/**
 * A dátum választóhoz tartozó input mező. Ha nincs ilyen, akkor null.
 */
let field = null;

/**
 * A dátum választóhoz tartozó input mező tartalma Date objektumként. Ha nincs ilyen, akkor null.
 */
let fieldDate = null;

/**
 * A dátum választó éve. Csak a 100-9999 tartományban lehet. 
 */
let year = null;

/**
 * A dátum választó hónapja. Csak az 1-12 tartományban lehet. 
 */
let month = null;

/**
 * A dátum választó nyelve.
 */
let language = null;

/**
 * A dátum formátuma.
 */
let format = null;

/**
 * Dátum konverziós példány 
 */
let dateConvert = null;

/**
 * Inicializálás. A "data-xesjslib-date-picker-activate" attribútummal ellátott elemek beállítása.
 */
let elements = document.querySelectorAll('['+ ACTIVATE_ATTRIBUTE + ']');
for (let elem of elements) {
  let activateId = elem.getAttribute(ACTIVATE_ATTRIBUTE);
  let activeElem = document.getElementById(activateId);  
  if (activeElem === null) {
    throw new XesjDataError(
      `Az egyik html-elem ${ACTIVATE_ATTRIBUTE} attribútuma nemlétező html-id értéket tartalmaz: ${activateId}`
    );
  }
  else {
    activeElem.addEventListener('click', () => showPicker(elem));
  }
}

/**
 * Dátum választó (naptár) létrehozása, és megjelenítése a képernyőn.
 * @param {Element} f Az a mező melyre a dátum választás vonatkozik.
 */
function showPicker(f) {

  // Régi naptár törlése
  removePicker();
  
  // Field beállítása
  field = f;
  
  // A field-hez tartozó nyelv lekérdezése, és a nyelv ellenőrzése
  language = field.getAttribute(LANGUAGE_ATTRIBUTE);
  if (language === null || language === '') language = DEFAULT_LANGUAGE;
  if (TITLE[language] === undefined) {
    throw new XesjDataError(
      `Az egyik html-elem ${LANGUAGE_ATTRIBUTE} attribútuma érvénytelen nyelvet tartalmaz: ${language}`
    );
  }
  
  // A field-hez tartozó formátum lekérdezése, és a formátum ellenőrzése
  format = field.getAttribute(FORMAT_ATTRIBUTE);
  if (format === null || format === '') format = DEFAULT_FORMAT;
  try {
    dateConvert = new DateConvert(format);
  }
  catch (error) {
    if (error instanceof XesjDataError) {
      throw new XesjDataError(
        `Az egyik html-elem ${FORMAT_ATTRIBUTE} attribútuma érvénytelen! ` + error.message
      );
    }
    else throw error;
  }
  
  // Naptár évének, és hónapjának beállítása
  fieldDate = null;
  try {
    fieldDate = dateConvert.stringToDate(field.value);
    if (fieldDate === null) {
      throw new XesjDataError('A mező üres!');
    }
    // A mező kitöltött, és helyes formában tartalmaz dátumot
    year = fieldDate.getFullYear();
    month = fieldDate.getMonth() + 1;
  }
  catch (error) {
    if (error instanceof XesjDataError) {
      // A mező üres, vagy rossz formában tartalmaz dátumot
      let today = new Date();
      year = today.getFullYear();
      month = today.getMonth() + 1;
    }
    else throw error;
  }
  
  // A mező pozíciója 
  let fieldRect = field.getBoundingClientRect();
  let fieldLeft = fieldRect.left + window.pageXOffset;
  let fieldBottom = fieldRect.bottom + window.pageYOffset;
  
  // Naptár létrehozása a template alapján
  document.body.insertAdjacentHTML('beforeend', PICKER_TEMPLATE);
  picker = document.getElementById('xesjslib-date-picker-id');

  // Naptár pozicionálása
  picker.style.position = 'absolute';
  picker.style.top = `${fieldBottom + 4}px`;
  picker.style.left = `${fieldLeft}px`;

  // A 7 nap betűinek beírása a naptár második sorába  
  for (let c = 0; c <= 6; c++) {
    let cellElem = picker.rows[1].cells[c];
    cellElem.innerHTML = TITLE[language].day[c];
  }
  
  // Naptár belső tartalmának frissítése
  calendarRefresh();

  // Click listener hozzárendelése a naptárhoz
  picker.addEventListener('click', cellClick);

  // Focusout listener hozzárendelése a naptárhoz: ha elveszti a fókuszt le kell venni a képernyőről
  picker.addEventListener('focusout', removePicker);

  // Fókusz ráhelyezése a naptárra
  picker.focus();
  
} // End: showPicker() 

/** 
 * Naptár tartalmának frissítése
 */
function calendarRefresh() {

  // Év, hónap felirat kitöltése
  let cellElem = picker.rows[0].cells[1];
  let monthText = TITLE[language].month[month - 1];
  cellElem.innerHTML = `${year} ${monthText}`;

  // Kezdőnap: 1
  let day = 1;

  // Hány üres cellával kell kezdeni ?
  let d = new Date(year, month - 1, 1).getDay();
  let space = (d === 0 ? 6 : d - 1);

  // Utolsó nap meghatározása
  let dayMax = new Date(year, month, 0).getDate();

  // Cellák kitöltése
  let today = new Date();
  for (let r = 2; r <= 7; r++) {
    for (let c = 0; c <= 6; c++) {
      cellElem = picker.rows[r].cells[c];
      if (space > 0 || day > dayMax) {
        // Üres cella
        space--;
        cellElem.innerHTML = '&nbsp;';
        cellElem.removeAttribute('data-pick-day');
        cellElem.className = 'passive';
      } 
      else {
        // Kitöltött cella
        cellElem.innerHTML = day;
        cellElem.setAttribute('data-pick-day', String(day));
        cellElem.className = 'active';
        // A mai nap?
        if (today.getFullYear() === year && today.getMonth() + 1 === month && today.getDate() === day) {
          cellElem.classList.add('fw-bold');
          cellElem.classList.add('text-primary');
        }
        // A választott nap?
        if (fieldDate !== null && fieldDate.getFullYear() === year && fieldDate.getMonth() + 1 === month
          && fieldDate.getDate() === day) {
          cellElem.classList.add('bg-primary-subtle');
        }
        day++;
      }
    }
  }

} // End: calendarRefresh()

/**
 * Klikkelés a naptár egy cellájára. 
 */
function cellClick(event) {

  // Hónap váltó cellára kattintás? 
  let shift = event.target.getAttribute('data-shift-month');
  if (shift !== null && shift !== '') {
    monthUpdate(Number(shift));
    return;
  }  

  // Dátum cellára kattintás? Akkor dátum meghatározása, és beírása az input mezőbe, naptár eltávolítása.
  let day = event.target.getAttribute('data-pick-day');
  if (day !== null && day !== '') {
    let date = new Date(year, month - 1, Number(day));
    field.value = dateConvert.dateToString(date);
    removePicker();
  }  

}

/** 
 * Hónap módosítása. 
 * Ha a hónap átfordul decemberről januárra, vagy a januárról decemberre, akkor változtatja az évet is.
 * @param {number} modify Hónap módosító, melynek értéke -1 vagy 1 
 */
function monthUpdate(modify) {

  // Előkészítés
  let newYear = year;
  let newMonth = month + modify;
  
  // Módosítás
  if (newMonth === 0) {
    newMonth = 12;
    newYear--;
  }
  if (newMonth === 13) {
    newMonth = 1;
    newYear++;
  }
  
  // Ellenőrzés, naptár frissítés 
  if (newYear >= 100 && newYear <= 9999) {
    year = newYear;
    month = newMonth;
    calendarRefresh();
  }

}

/**
 * Dátum választó törlése a html oldalról.
 * A Chrome, és Edge böngészők miatt előszőr a picker-t kell null-ozni,
 * mert a remove() rögtön kiváltja a picker focusout eseményét is, ami szintén meghívja ezt a metódust.
 */
function removePicker() {
  
  if (picker !== null) {
    let p = picker;
    picker = null;
    p.remove();
  } 

}

/******************************************************************************************************************************/