/*******************************************************************************************************************************
 *
 * Modul: javascript alap típusok ellenőrzése, és konverziók végrehajtása az alap típusokon.
 * 
 * Ezeket az alap típusokat kezeli:
 * 
 *   - number
 *   - string
 *   - boolean
 *   - object
 *   - function
 *   - bigint
 *   - undefined
 *   - symbol
 *  
 ******************************************************************************************************************************/

// Import
import {XesjDataError} from './error.js';

/**
 * Vizsgálja, hogy az adat string-e ?
 * @param {any} data A vizsgálandó adat
 * @return {boolean} True: ha az adat string vagy null. Különben false.
 */
export function isString(data) {
  
  return (data === null || typeof(data) === 'string');

}

/**
 * Vizsgálja, hogy az adat boolean-e ?
 * @param {any} data A vizsgálandó adat
 * @return {boolean} True: ha az adat boolean vagy null. Különben false.
 */
export function isBoolean(data) {
  
  return (data === null || typeof(data) === 'boolean');

}

/**
 * Vizsgálja, hogy az adat object-e ?
 * @param {any} data A vizsgálandó adat
 * @return {boolean} True: ha az adat object vagy null. Különben false.
 */
export function isObject(data) {
  
  return (data === null || typeof(data) === 'object');

}

/**
 * Vizsgálja, hogy az adat tömb-e ?
 * @param {any} data A vizsgálandó adat
 * @return {boolean} True: ha az adat tömb vagy null. Különben false.
 */
export function isArray(data) {
  
  return (data === null || Array.isArray(data));

}

/**
 * String konvertálása igazi számra. 
 * A string nem tartalmazhat space, "e", "E" karaktereket.
 * Konvertálási esetek:
 *   null     ->  null
 *   ''       ->  null
 *   '12'     ->  12
 *   '-5.67'  ->  -5.67
 *   '0013.8' ->  13.8
 *   ' 12'    ->  XesjDataError, mivel szóközt tartalmaz
 *   '1e2'    ->  XesjDataError, mivel "e" betűt tartalmaz
 * @param {string} data Konvertálandó adat. Null vagy string lehet.
 * @returns {number} Konvertált adat. Igazi szám, vagy null. 
 * @throws {XesjDataError} Ha rossz a konvertálandó adat típus, vagy nem alakítható át igazi számmá.
 */
export function stringToNumber(data) {
  
  // Paraméter típusa string?
  if (!isString(data)) {
    throw new XesjDataError(`Az adat nem string: ${data}`);
  }
  
  // Null vagy üres string?
  if (data === null || data === '') return null;
  
  // A string tartalmaz tiltott karaktereket (space, "e", "E")?
  if (data.indexOf(' ') > -1 || data.indexOf('e') > -1 || data.indexOf('E') > -1) {
    throw new XesjDataError(`Az adat nem konvertálható számra: ${data}`);
  }
  
  // Átalakítás
  let result = Number(data);
  
  // Eredmény vizsgálata, NaN, Infinity, -Infinity kizárva.
  if (isNaN(result) || !isFinite(result)) {
    throw new XesjDataError(`Az adat nem konvertálható számra: ${data}`);
  }
  
  // Válasz
  return result;
  
}

/**
 * String konvertálása igazi egész számra.
 * A string nem tartalmazhat space, "e", "E" karaktereket.
 * Konvertálási esetek:
 *   null     ->  null
 *   ''       ->  null
 *   '12'     ->  12
 *   '-5'     ->  -5
 *   '0013'   ->  13
 *   '7.0'    ->  7
 *   ' 12'    ->  XesjDataError, mivel szóközt tartalmaz
 *   '1e2'    ->  XesjDataError, mivel "e" betűt tartalmaz
 * @param {string} data Konvertálandó adat. Null vagy string lehet.
 * @returns {number} Konvertált adat. Igazi egész szám, vagy null. 
 * @throws {XesjDataError} Ha rossz a konvertálandó adat típus, vagy nem alakítható át igazi egész számmá.
 */
export function stringToInteger(data) {
  
  // Átalakítás
  let result = stringToNumber(data);
  
  // Egész szám az eredmény?
  if (result !== null) {
    if (Math.round(result) !== result) {
      throw new XesjDataError(`Az adat nem konvertálható egész számra: ${data}`);
    }
  }
  
  // Válasz
  return result;
  
}

/**
 * String konvertálása igazi számra, ahol a string végződése lehet a következő is:
 *   "K" = kilo (a szám értéke ennyivel lesz szorozva: 1024)
 *   "M" = mega (a szám értéke ennyivel lesz szorozva: 1024^2)
 *   "G" = giga (a szám értéke ennyivel lesz szorozva: 1024^3)
 *   "T" = tera (a szám értéke ennyivel lesz szorozva: 1024^4)
 *   "P" = peta (a szám értéke ennyivel lesz szorozva: 1024^5)
 * A string nem tartalmazhat space, "e", "E" karaktereket.
 * Konvertálási esetek:
 *   null     ->  null
 *   ''       ->  null
 *   '12'     ->  12
 *   '-0.5K'  ->  -512
 * @param {string} data Konvertálandó adat. Null vagy string lehet.
 * @returns {number} Konvertált adat. Igazi szám, vagy null. 
 * @throws {XesjDataError} Ha rossz a konvertálandó adat típus, vagy nem alakítható át igazi számmá.
 */
export function stringToNumberKMG(data) {
  
  // Előkészítés
  let szorzo = 1;
  const KMG = 'KMGTP';
  
  // A végződés vizsgálata
  if (typeof(data) === 'string' &&  data.length >= 2) {
    let index = KMG.indexOf(data.at(-1));
    if (index > -1) {
      szorzo = Math.pow(1024, index + 1);
      data = data.substring(0, data.length - 1);
    }  
  }
  
  // Átalakítás
  let result = stringToNumber(data);
  
  // Válasz
  return (result === null ? null : result * szorzo);
  
}

/**
 * String konvertálása boolean-ra. 
 * A string csak null, üres string, "true", "false" lehet.
 * Konvertálási esetek:
 *   null     ->  null
 *   ''       ->  null
 *   'true'   ->  true
 *   'false'  ->  false
 *   'TRUE'   ->  XesjDataError
 * @param {string} data Konvertálandó adat. Null vagy string lehet.
 * @returns {boolean} Konvertált adat.  
 * @throws {XesjDataError} Ha rossz a konvertálandó adat típus, vagy nem alakítható át boolean-ra.
 */
export function stringToBoolean(data) {
  
  // Paraméter típusa string?
  if (!isString(data)) {
    throw new XesjDataError(`Az adat nem string: ${data}`);
  }
  
  // Válasz
  if (data === null || data === '') return null;
  if (data === 'true') return true;
  if (data === 'false') return false;
  throw new XesjDataError(`Az adat nem konvertálható boolean-ra: ${data}`);
  
}

/******************************************************************************************************************************/