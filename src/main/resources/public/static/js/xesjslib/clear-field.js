/*******************************************************************************************************************************
 *
 * Modul: mezők tartalmának törlése. 
 * A törlendő mezők csak a következők lehetnek:
 * 
 *   <input type="text>
 *   <input type="password">
 *   <input type="hidden">
 *   <input type="radio">
 *   <input type="checkbox">
 *   <input type="file">
 *   <textarea>
 *   <select>
 * 
 * A mezők törléséhez szükséges egy "törlő" html elem, mely a törlést végrehajtja egy eseményre.
 * A weboldalon több ilyen törlő-elem lehet, mely tipikusan gomb vagy link, de lehet más is.
 * A törlő elemet el kell látni a következő attribútumokkal:
 * 
 *   - data-xesjslib-clear-field-event
 *     Kötelező megadni. Meghatározza, hogy a törlő elemen milyen eseménynek kell bekövetkeznie, hogy a törlést végrehajtsa. 
 *     Példa: data-xesjslib-clear-field-event="click"
 *
 *   - data-xesjslib-clear-field-target
 *     Css-selector. Leírja hogy mely elemek tartalmát kell törölni. Nem kötelező.
 *     Ha nincs megadva, akkor nem történik törlés.
 *     Példa: azon mezők tartalmának törlése, melyek rendelkeznek "data-torlendo" attribútummal, vagy az id-je "id123": 
 *     data-xesjslib-clear-field-target="[data-torlendo], #id123"
 *     Nem okoz problémát ha nem létező elemre hivatkozik a css-selector.
 * 
 * A modul exportálja a clearField() függvényt, mely a törlést végzi, így máshol is felhasználható. 
 * Ez akkor hasznos, ha speciális esemény bekövetkezésekor kell a mezőket törölni, például 10 percenként. 
 * Erre saját javascriptet kell írni, de a clearField() meghívható más modulból is.
 * 
 * Teljes példa:
 * 
 *   <button type="button" 
 *           data-xesjslib-clear-field-event="click" 
 *           data-xesjslib-clear-field-target="[data-torlendo]">törlés</button>
 *   <input type="text" value="árvíztűrő" data-torlendo/>
 *   <textarea data-torlendo></textarea>
 *   
 * A modul használata: a html-oldalon hivatkozni kell erre a modulra: 
 * 
 *   <script type="module" src=".../clear-field.js"></script>
 * 
 * A modul a lefutásakor automatikusan felderíti a "data-xesjslib-clear-field-event" attribútummal ellátott html elemeket, 
 * és elvégzi a szükséges teendőket. 
 * 
 ******************************************************************************************************************************/

// Import
import {XesjError, XesjDataError} from './error.js';

// A "data-xesjslib-clear-field-event" attribútummal rendelkező elemek megkeresése
let elements = document.querySelectorAll('[data-xesjslib-clear-field-event]');

// Ciklus az elemeken 
for (let elem of elements) {

  // Ellenőrzés: a "data-xesjslib-clear-field-event" értéke ki van töltve?
  let event = elem.getAttribute('data-xesjslib-clear-field-event');
  if (event === null || event === '') {
    throw new XesjDataError('A "data-xesjslib-clear-field-event" attribútum értéke nincs kitöltve!');
  }
  
  // Event listener hozzárendelése az elemhez
  elem.addEventListener(event, eventHandler);

}

/**
 * Event handler
 */
function eventHandler(event) {
  
  // A törlő elem
  let elem = event.currentTarget;
  
  // Target meghatározása
  let target = elem.getAttribute('data-xesjslib-clear-field-target');
  
  // Ha a target meg van adva, akkor a target által meghatározott mezők törlése
  if (target !== null && target !== '') clearField(target);
  
}

/**
 * Űrlap mezők tartalmának törlése.
 * @param {string} selector Törlendő mezők css-selectora.
 */
export function clearField(selector) {

  // Törlendő elemek megkeresése
  let elements = document.querySelectorAll(selector);

  // Ciklus az elemeken
  for (let elem of elements) {
    if (elem.tagName === 'INPUT' && ['text', 'password', 'hidden', 'file'].includes(elem.type)) {
      elem.value = null;
    }
    else if (elem.tagName === 'INPUT' && ['radio', 'checkbox'].includes(elem.type)) {
      elem.checked = false;
    }
    else if (elem.tagName === 'SELECT' || elem.tagName === 'TEXTAREA') {
      elem.value = null;
    }
    else {
      throw new XesjError(`Ennek a html elemnek nem törölhető a tartalma: <${elem.tagName.toLowerCase()}>`);
    }
  }

};

/******************************************************************************************************************************/