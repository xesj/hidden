/*******************************************************************************************************************************
 *
 * A modul ezen funkciókat végzi el:
 *   - Lezárás idejének kiírása a képernyőre (a sarokba, és a lezárás modal ablakba is).
 *   - Lezárás modal ablak megjelenítése (ha a lezárási időből kevés van hátra).
 *   - Navigálás a lezárás oldalra ha az idő lejárt.
 *   - Backend ping-elése (hogy ne járjon le a backend session timeout).
 *   - Lezárás linkre való kattintás lekezelése (a lezárási idő újraindítása).
 * A modal ablak lezárható az ESC billentyűvel, a "Tovább" gombjára kattintással, és a bezárás gombjára kattintással.
 * A modal ablak nem zárható le ha mellé kattintunk.
 * A modal ablak felhasználói lezárásakor a lezárási idő újraindul.
 *
 * Használat: a html-oldalon csak hivatkozni kell erre a javascript modulra: 
 *
 *   <script type="module" src=".../logout.js"></script>
 *
 *   A modul elején lévő változókat be kell állítani a helyes működéshez! 
 *
 ******************************************************************************************************************************/

// Import
import {httpServiceCall} from './xesjslib/service.js';
import {XesjError} from './xesjslib/error.js';

// Ezzel a css-selector-ral ellátott elem(ek) belsejébe kell beírni a lezárási időt
const printCssSelector = '.logoutTime'; 

// Erre az url-re kell navigálni, ha lejárt az idő
const logoutPath = document.getElementById('logoutPath').textContent;

// Ezt az url-t kell hívni a backend ping-eléshez
const pingPath = document.getElementById('pingPath').textContent;

// Session timeout (másodpercben)
const sessionTimeout = document.getElementById('sessionTimeout').textContent; 

// Modal ablak megjelenítés ideje, ha ennyi vagy kevesebb idő van hátra (másodpercben)
const modalTimeout = 60;

// Modal ablak css-selector 
const modalCssSelector = '#logoutModal';

// Modal ablak látszik a képernyőn ? 
let modalShow = false;

// Modal elem megkeresése, meglétének ellenőrzése
const modalElement = document.querySelector(modalCssSelector);
if (modalElement === null) {
  throw new XesjError(`Nem létezik modal ablak ilyen css-selector-ral: "${modalCssSelector}"`);
}

// Azoknak az elemeknek a meghatározása melyeknek a belsejébe ki kell írni az időt
let printElements = document.querySelectorAll(printCssSelector);

// Modal-ra bezárás esemény figyelése
modalElement.addEventListener('hidden.bs.modal', function () {
  reset();
  modalShow = false;
});

// StartTime beállítása
let startTime = Date.now();

// Repeater indítása
repeater();

/**
 * Repeater függvény
 */
function repeater() {
  
  handler();
  setTimeout(repeater, 1000);

}

/**
 * Idő kiírása a képernyőre, modal ablak megjelenítése, navigálás a lezárás oldalra
 */
function handler() {
  
  // RealTimeout: a hátralévő idő a session timeout-ból (másodpercben megadva)
  var currentTime = Date.now();
  var realTimeout = sessionTimeout - Math.floor((currentTime - startTime) / 1000);
  
  // Negatív érték esetén navigálás a lezárás oldalra
  if (realTimeout <= 0) {
    window.location.href = logoutPath;
  }
  
  // Modal ablak megjelenítése ha szükséges
  if (!modalShow && realTimeout <= modalTimeout) {
    let modal = new globalThis.bootstrap.Modal(modalElement, { backdrop: 'static', keyboard: true });
    modal.show();
    modalShow = true;
  }
  
  // Óra meghatározása a realTimeout-ból
  let hour = Math.floor(realTimeout / 3600);
  realTimeout -= hour * 3600;
  
  // Perc meghatározása a realTimeout-ból
  let minute = Math.floor(realTimeout / 60);
  realTimeout -= minute * 60;

  // Másodperc meghatározása a realTimeout-ból
  let second = realTimeout;
  
  // Kiírandó idő meghatározása
  let textTimeout;
  if (hour > 0) {
    textTimeout = hour + ':' + (minute < 10 ? '0' + minute : minute) + ':' + (second < 10 ? '0' + second : second);
  }
  else {
    textTimeout = minute + ':' + (second < 10 ? '0' + second : second);
  }

  // Kiírás minden szükséges elem belsejébe
  for (let elem of printElements) {
    elem.textContent = textTimeout;
  }

}

/**
 * Reset: timeout újraindítása, backend ping-elése
 */
async function reset() {
  
  // Timeout újraindítás
  startTime = Date.now();
  
  // Backend ping-elés
  await httpServiceCall(pingPath);

}

/******************************************************************************************************************************/