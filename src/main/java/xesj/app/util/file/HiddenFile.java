package xesj.app.util.file;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import lombok.Getter;
import lombok.NonNull;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.exception.BadFileNameException;
import xesj.app.util.exception.ConcurrentException;
import xesj.app.util.exception.DecodeException;
import xesj.app.util.exception.FileSizeException;
import xesj.app.util.exception.HiddenException;
import xesj.app.util.exception.UnitException;
import xesj.app.util.main.SpBean;
import xesj.app.util.password.Password;
import xesj.tool.ByteTool;
import xesj.tool.LocaleTool;
import xesj.tool.RandomTool;

/**
 * Hidden fájl.
 */
@Getter
public class HiddenFile {

  /**
   * Fájlnév teljes egészében kiterjesztéssel együtt, de path nélkül.
   */
  private String name;

  /**
   * Aktuális fájl ?
   */
  private boolean actual;
  
  /**
   * A fájlhoz tartozó jelszó.
   */
  private Password password;

  /**
   * Bejegyzések a fájlban.
   * Kulcs: bejegyzés neve.
   * Érték: bejegyzés szövege.
   */
  private Map<String, String> notes;
  
  /**
   * Információ az "old" könyvtárba mozgatáshoz: forrás fájl
   */
  private File renameFile;
  
  /**
   * Információ az "old" könyvtárba mozgatáshoz: cél fájl
   */
  private File renameOldFile;
  
  /**
   * Konstruktor meglévő fájlhoz.
   * @param name Fájlnév path nélkül. Formátum: {baseName}-1999-12-31T23-59-59.hidden
   * @param actual Aktuális fájl ?
   * @throws BadFileNameException Ha a fájlnév formátuma nem megfelelő.
   */
  public HiddenFile(@NonNull String name, boolean actual) throws BadFileNameException {

    // Előkészítés
    final String ERROR_MESSAGE = "A fájlnév formátuma helytelen: " + name;
    this.name = name;
    this.actual = actual;
    
    // 1. ellenőrzés: megfelel a pattern-nek ?
    Matcher matcher = Constant.FILE_NAME_PATTERN.matcher(name);
    if (!matcher.matches()) {
      throw new BadFileNameException(ERROR_MESSAGE);
    }    

    //  2. ellenőrzés: LocalDateTime-ra alakítható a dátum része a fájlnévnek ?
    try {
      getLocalDateTime();
    }
    catch (DateTimeParseException dtpe) {
      throw new BadFileNameException(ERROR_MESSAGE);
    }
    
  }
  
  /**
   * Konstruktor új fájlhoz.
   * @param baseName Fájlnév kezdete, amit a felhasználó adott meg (dátum előtti rész).
   * @param password A fájlhoz tartozó jelszó, amit a felhasználó szeretne használni.
   * @param passwordType A fájlhoz tartozó jelszó típusa, amit a felhasználó szeretne használni.
   */
  public HiddenFile(@NonNull String baseName, @NonNull String password, @NonNull Password.Type passwordType) {
    
    // HiddenFile előállítása
    this(nameNow(baseName), true);
    
    // Password beállítása
    this.password = new Password(password, passwordType);
    
    // Bejegyzés beállítása
    notes = new HashMap<>();
    
  }
  
  /**
   * Egyenlőség vizsgálata
   */
  @Override
  public boolean equals(Object object) {

    // Hidden típusú ?
    if (!(object instanceof HiddenFile)) return false;
    
    // Egyenlők a 'name' és az 'actual' értékek ? 
    HiddenFile h = (HiddenFile)object;
    return (h.getName().equals(name) && h.isActual() == actual);

  }
  
  /**
   * Hashcode. Az equals() implementálása miatt kell.
   */
  @Override
  public int hashCode() {
    
    return Objects.hashCode(name) + Objects.hashCode(actual);
    
  }

  /**
   * Valódi fájl lekérdezése.
   */
  public File getFile() {
    
    // File objektum előállítása
    Path path = Paths.get(
      SpBean.property().getBaseDir(), 
      SpBean.session().getTicket().getId(), 
      (actual ? "" : "old"), 
      name
    );
    return path.toFile();
    
  } 
  
  /**
   * A fájl már nyitva van a MainSession-ben ?
   */
  public boolean isOpened() {
    
    MainSession session = SpBean.session();
    if (!session.hasOpenedFile()) {
      return false; // Nincs a session-ben nyitott fájl
    }
    return this.equals(session.getOpenedFile());
    
  } 
  
  /**
   * Fájl ellenőrzése: Létezik ? Olvasható ? Támogatott verzió ?
   * @throws HiddenException A fájl nem létezik, vagy nem olvasható, vagy nem támogatott a verziója.
   * @return A fájl verziója (7. bájtja)
   */
  public int checkFile() throws FileNotFoundException, IOException {

    // Előkészítés
    File file = getFile();
    
    // A fájl létezik ?
    if (!file.exists()) {
      throw new HiddenException("A fájl nem létezik: " + file.getCanonicalPath());
    }

    // A fájl olvasható ?
    if (!file.canRead()) {
      throw new HiddenException("A fájl nem olvasható: " + file.getCanonicalPath());
    }
    
    // Támogatott verzió ?
    int version;
    try (FileInputStream fis = new FileInputStream(file)) {
      byte[] bytes = new byte[7];
      int length = fis.read(bytes);
      if (length == 7 && 
          bytes[0] == Constant.FILE_START[0] && bytes[1] == Constant.FILE_START[1] && bytes[2] == Constant.FILE_START[2] && 
          bytes[3] == Constant.FILE_START[3] && bytes[4] == Constant.FILE_START[4] && bytes[5] == Constant.FILE_START[5]) {
        // A fájl első 7 byte-ja: "hiddenV"
        version = ByteTool.byteToInt(bytes[6]);
      }
      else {
        throw new HiddenException("A fájl verziója nem határozható meg: " + file.getCanonicalPath());
      }
    }
    
    // A verzió megfelelőségének ellenőrzése
    if (!Constant.OPEN_FILE_VERSIONS.contains(version)) {
      throw new HiddenException(
        "A fájl verziója: " + version + ". Ez a verzió nem támogatott! Fájl: " + file.getCanonicalPath()
      );
    }
    
    // Válasz
    return version;

  }
  
  /**
   * Fájl megnyitása (dekódolása).
   * @param password Jelszó a dekódoláshoz.
   * @throws DecodeException Ha a dekódolás sikertelen a jelszó által.
   */
  public void open(Password password) throws DecodeException, Exception {

    // A fájl ellenőrzése
    int version = checkFile();
    
    // Fájl objektum
    File file = getFile();

    // Fájl elemei
    byte[] ignoreBytes = new byte[7];
    byte[] gcmIvBytes = new byte[16];
    byte[] encodedBytes = new byte[(int)file.length() - ignoreBytes.length - gcmIvBytes.length];
    try (FileInputStream inputStream = new FileInputStream(file)) {
      inputStream.read(ignoreBytes);
      inputStream.read(gcmIvBytes);
      inputStream.read(encodedBytes);
    }

    // GCM paraméter előállítása
    GCMParameterSpec gcmParameter = new GCMParameterSpec(128, gcmIvBytes);
    
    // SecretKey választása a fájl baseName végződése alapján
    SecretKey secretKey = (isSalt0() ? password.getSecretKeySalt0() : password.getSecretKey());
    
    // Dekódolás
    Cipher cipher = Cipher.getInstance(Constant.CIPHER_ALGORITHM);
    cipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameter);
    byte[] decodedBytes;
    try {
      decodedBytes = cipher.doFinal(encodedBytes);
    }  
    catch (IllegalBlockSizeException | BadPaddingException e) {
      throw new DecodeException("A jelszóval a fájl nem nyitható meg!");
    }

    // Az első 16 ellenőrző byte vizsgálata
    if (decodedBytes.length < 16) {
      throw new DecodeException("A dekódolt bájt-sorozat hossza kevesebb mint 16 byte!");
    }
    for (int i = 0; i <= 7; i++) {
      int a = ByteTool.byteToInt(decodedBytes[i]);
      int b = ByteTool.byteToInt(decodedBytes[i + 8]);
      if (a + b != 255) {
        throw new DecodeException("A dekódolás során hibás ellenőrző bájtok keletkeztek!");
      }
    }

    // Bejegyzések felépítése, az első 16 byte figyelmen kívül hagyásával  
    notes = new HashMap<>();
    String s = new String(decodedBytes, 16, decodedBytes.length - 16, StandardCharsets.UTF_8);
    String[] array = s.split(Constant.NOTES_SEPARATOR, -1);
    for (int i = 0; i < array.length - 1; i += 2) {
      String noteName = array[i];
      String noteText = array[i + 1];
      if (noteText.isEmpty()) {
        noteText = null;
      }
      notes.put(noteName, noteText);
    }
    
    // Helyes jelszó beállítása
    this.password = password;

  }
  
  /**
   * Bejegyzések nevek lekérdezése abc-sorrendben.
   * @param fragment Bejegyzés név szótöredék. Null esetén nincs szűrés. 
   */
  public List<String> getNoteNames(String fragment) {
    
    // Fragment tisztítás
    final String fragmentSearch = (fragment == null ? null : fragment.trim().toLowerCase());
    
    // Lista összeállítása
    return notes.keySet().stream()
      .filter(s -> fragmentSearch == null || s.toLowerCase().contains(fragmentSearch))
      .sorted(Collator.getInstance(LocaleTool.LOCALE_HU))
      .collect(Collectors.toList());
    
  }

  /**
   * Fájl mentése.
   * @throws UnitException Ha a mentéssel túllépnénk a megengedett hidden fájl darabszámot.
   * @throws FileSizeException Ha a mentéssel túllépnénk a megengedett hidden fájl méretet.
   */
  public void save() throws UnitException, FileSizeException, Exception {
    
    // Bean-ek elérése
    MainSession session = SpBean.session();
    
    // Fájl darabszám ellenőrzése
    int actualUnit = getFiles(true, null).size();
    int oldUnit = getFiles(false, null).size();
    if (actualUnit + oldUnit >= session.getTicket().getUnit()) {
      throw new UnitException(actualUnit, oldUnit);
    } 

    // Bejegyzések összefűzése egy stringbe, majd UTF-8 szerint bájtsorozattá alakítása.
    // Formátum: "bejegyzés1|szöveg1|bejegyzés2|szöveg2|bejegyzés3||bejegyzés4|szöveg4"
    StringBuilder builder = new StringBuilder();
    for (String key: notes.keySet()) {
      String text = Optional.ofNullable(notes.get(key)).orElse("");
      if (builder.length() > 0) builder.append(Constant.NOTES_SEPARATOR);
      builder.append(key).append(Constant.NOTES_SEPARATOR).append(text);
    }
    byte[] noteBytes = builder.toString().getBytes(StandardCharsets.UTF_8);
    
    // Hitelesítő bájtsorozat előállítása (16 bájt)
    byte[] authBytes = getAuthBytes();

    // Bájtsorozatok összefűzése: decodedBytes = authBytes + noteBytes 
    byte[] decodedBytes = new byte[noteBytes.length + authBytes.length];
    System.arraycopy(authBytes, 0, decodedBytes, 0, authBytes.length);
    System.arraycopy(noteBytes, 0, decodedBytes, 16, noteBytes.length);

    // GCM paraméter előállítása (mindig más)
    byte[] gcmIvBytes = new byte[16];
    new SecureRandom().nextBytes(gcmIvBytes);
    GCMParameterSpec gcmParameter = new GCMParameterSpec(128, gcmIvBytes);
    
    // SecretKey választása a fájl baseName végződése alapján
    SecretKey secretKey = (isSalt0() ? password.getSecretKeySalt0() : password.getSecretKey());
    
    // Titkosítás
    Cipher cipher = Cipher.getInstance(Constant.CIPHER_ALGORITHM);
    cipher.init(Cipher.ENCRYPT_MODE, secretKey, gcmParameter);
    byte[] encodedBytes = cipher.doFinal(decodedBytes);
    
    // Fájlméret ellenőrzése
    int fileSize = 23 + encodedBytes.length;
    if (fileSize > session.getTicket().getSizeBytes()) {
      throw new FileSizeException(fileSize);
    }

    // Új érték a 'name' változóba
    name = nameNow(getBaseName());
    
    // Írás a fájlrendszerbe
    File file = getFile();
    try (FileOutputStream stream = new FileOutputStream(file)) {
      stream.write(Constant.FILE_START); // "hidden" bájtsorozat kiírása
      stream.write(ByteTool.intToByte(Constant.SAVE_FILE_VERSION));  // verzió kiírása
      stream.write(gcmIvBytes); // GCM Initialization Vector
      stream.write(encodedBytes);
    }

    // A régi fájl áthelyezése az "old" könyvtárba, ha az áthelyezési információk ki vannak töltve
    if (renameFile != null && renameOldFile != null) {
      renameFile.renameTo(renameOldFile);
      renameFile = null;
      renameOldFile = null;
    }  

  }
  
  /**
   * Fájl információk letárolása ahhoz, hogy később a fájlt a save() metódus át tudja helyezni az "old" könyvtárba.
   * Ellenőrzi, hogy a fájl megvan-e a fájlrendszerben. 
   * @throws ConcurrentException Ha a fájl már nem létezik a fájlrendszerben.
   */
  public void oldInfo() throws ConcurrentException {
    
    // Információk tárolása a későbbi áthelyezéshez, de csak akkor ha még nincs kitöltve
    if (renameFile == null) {
      renameFile = getFile();
      Path path = Paths.get(
        SpBean.property().getBaseDir(), 
        SpBean.session().getTicket().getId(), 
        "old", 
        name
      );
      renameOldFile = path.toFile();
    }  

    // Fájl létezésének ellenőrzése
    if (!renameFile.exists()) {
      throw new ConcurrentException("A fájl nem található a fájlrendszerben!");
    }
    
  }
  
  /**
   * Létezik-e a bejegyzés név ?
   * @param noteName Bejegyzés neve.
   */
  public boolean hasNote(@NonNull String noteName) {
    
    return notes.containsKey(noteName);
    
  }

  /**
   * Fájl törlése a fájlrendszerből 
   */
  public void deleteFile() {
    
    getFile().delete();

  }

  /**
   * Fájl jellemzők módosítása: fájlnév, jelszó.
   * Csak olyan adatot módosít, melyet megkap nem null paraméterként.
   * @param baseName Fájlnév kezdete, amit a felhasználó adott meg.
   */
  public void property(String baseName, String password, Password.Type passwordType) {
    
    // Fájlnév módosítása, ha szükséges
    if (baseName != null) {
      this.name = nameNow(baseName);
    }  
    
    // Jelszó módosítása, ha szükséges
    if (password != null) {
      this.password = new Password(password, passwordType);
    }  
    
  }
  
  /**
   * BaseName lekérdezése.
   */
  public String getBaseName() {
    
    return name.substring(0, name.length() - 27);
    
  }
  
  /**
   * Salt0-ás fájl, vagyis a baseName végződése "-salt0" ? 
   */
  public boolean isSalt0() {
    
    return getBaseName().endsWith(Constant.HIDDEN_FILE_BASENAME_END_SALT0);
    
  }

  /**
   * A fájlnévben szereplő dátum lekérdezése LocalDateTime objektumként.
   * @throws DateTimeParseException Ha nem alakítható át LocalDateTime objektumra.
   */
  public LocalDateTime getLocalDateTime() throws DateTimeParseException {
    
    String fragment = name.substring(name.length() - 26, name.length() - 7);
    return LocalDateTime.parse(
      fragment, 
      Constant.DATE_FORMAT_HIDDEN_FILE_NAME.withResolverStyle(ResolverStyle.STRICT)
    );
    
  };
  
  /**
   * A fájlnévben szereplő dátum lekérdezése ebben a formátumban: "éééé.hh.nn óó:pp:mm" 
   */
  public String getFormattedDate() {
    
    return getLocalDateTime().format(Constant.DATE_FORMAT_YMD_HMS);
    
  }
  
  /**
   * A baseName alapján előállítja a teljes nevet úgy, hogy abban az aktuális dátum szerepel
   */
  private static String nameNow(String baseName) {
    
    return baseName + "-" + LocalDateTime.now().format(Constant.DATE_FORMAT_HIDDEN_FILE_NAME) + ".hidden";
    
  }
  
  /**
   * Aktuális, vagy régi hidden fájlok listája.
   * Sorrend: fájl base-név abc-sorrend, ezen belül dátum szerint csökkenő sorrend.
   * @param actual Aktuális hidden fájlok ?
   * @param fragment Fájlnév szótöredék. Csak a HiddenFile.baseName -re szűr. Null esetén nincs szűrés. 
   * @return Hidden fájlok. Ha nincs ilyen, akkor üres lista. 
   */
  public static List<HiddenFile> getFiles(boolean actual, String fragment) {
    
    // Fragment átalakítása a kereséshez
    final String fragmentSearch = (fragment == null ? null : fragment.trim().toLowerCase());
    
    // Lista összeállítása
    Path path = Paths.get(
      SpBean.property().getBaseDir(), 
      SpBean.session().getTicket().getId(), 
      (actual ? "" : "old")
    );
    try {
      return Files
        .list(path)
        .map(p -> p.toFile().getName())
        .filter(n -> n.endsWith(".hidden"))
        .map(n -> new HiddenFile(n, actual))
        .filter(h -> fragmentSearch == null || h.getBaseName().contains(fragmentSearch))
        .sorted((h1, h2) -> { 
          var c = h1.getBaseName().compareTo(h2.getBaseName());
          return (c != 0 ? c : h2.getLocalDateTime().compareTo(h1.getLocalDateTime()));
        })
        .collect(Collectors.toList());
    }
    catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }
    
  }
  
  /**
   * Hitelesítő bájtsorozat előállítása (16 bájt).
   * A bájtsorozat első 8 bájtja bitenkénti ellentéte a második 8 bájtnak.  
   */
  public static byte[] getAuthBytes() {
    
    byte[] authBytes = new byte[16];
    for (int i = 0; i <= 7; i++) {
      int random = RandomTool.interval(0, 255);
      authBytes[i] = ByteTool.intToByte(random);
      authBytes[i + 8] = ByteTool.intToByte(255 - random);
    }
    return authBytes;
    
  }
  
  // ====
}
