package xesj.app.util.exception;

/**
 * Hibás hidden fájlnév esetén keletkező exception.
 */
public class BadFileNameException extends HiddenException {
  
  /**
   * Konstruktor
   */
  public BadFileNameException(String message) {

    super(message);
    
  }
  
  // ====
}
