package xesj.app.util.exception;

/**
 * Hibás ticket esetén keletkező exception.
 */
public class BadTicketException extends HiddenException {
  
  /**
   * Konstruktor
   */
  public BadTicketException(String message) {

    super(message);
    
  }
  
  // ====
}
