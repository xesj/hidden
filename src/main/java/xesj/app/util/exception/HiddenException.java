package xesj.app.util.exception;

/**
 * A program által kiváltott exception-ök őse. 
 */
public class HiddenException extends RuntimeException {
  
  /**
   * Konstruktor
   */
  public HiddenException(String message) {

    super(message);
    
  }

  // ====
}
