package xesj.app.util.exception;

/**
 * Hidden fájlok darabszámának túllépése miatt keletkező exception.
 */
public class UnitException extends HiddenException {
  
  public int actualUnit;
  public int oldUnit;
  
  /**
   * Konstruktor.
   * @param actaulUnit Aktuális fájlok száma.
   * @param oldUnit Régi fájlok száma.
   */
  public UnitException(int actualUnit, int oldUnit) {
 
    super("A fájlok darabszáma túl sok!");
    this.actualUnit = actualUnit;
    this.oldUnit = oldUnit;
    
  }
  
  // ====
}
