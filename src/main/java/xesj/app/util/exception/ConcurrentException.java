package xesj.app.util.exception;

/**
 * Egyazon hidden fájl konkurrens hozzáférése miatt keletkező exception. 
 */
public class ConcurrentException extends HiddenException {
  
  /**
   * Konstruktor.
   */
  public ConcurrentException(String message) {
 
    super(message);
    
  }
  
  // ====
}
