package xesj.app.util.exception;

/**
 * Hidden fájl túl nagy mérete miatt keletkező exception.
 */
public class FileSizeException extends HiddenException {
  
  public long fileSize;
  
  /**
   * Konstruktor.
   * @param fileSize Fájl mérete bájtban megadva.
   */
  public FileSizeException(long fileSize) {
 
    super("A fájl mérete túl nagy: " + fileSize + " bájt!");
    this.fileSize = fileSize;
    
  }
  
  // ====
}
