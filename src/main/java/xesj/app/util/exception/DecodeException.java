package xesj.app.util.exception;

/**
 * Hidden fájl hibás dekódolásakor keletkező exception.
 */
public class DecodeException extends HiddenException {
  
  /**
   * Konstruktor
   */
  public DecodeException(String message) {

    super(message);
    
  }
  
  // ====
}
