package xesj.app.util.exception;

/**
 * Hibás jelszó esetén keletkező exception.
 */
public class BadPasswordException extends HiddenException {
  
  /**
   * Konstruktor
   */
  public BadPasswordException(String message) {

    super(message);
    
  }
  
  // ====
}
