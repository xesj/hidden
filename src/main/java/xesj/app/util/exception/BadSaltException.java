package xesj.app.util.exception;

/**
 * Hibás salt esetén keletkező exception.
 */
public class BadSaltException extends HiddenException {
  
  /**
   * Konstruktor
   */
  public BadSaltException(String message) {

    super(message);
    
  }
  
  // ====
}
