package xesj.app.util.main;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xesj.app.system.MainSession;
import xesj.app.util.exception.HiddenException;

/**
 * Általános ellenőrzések. Ha hibát talál, akkor mindig HiddenException-t dob.
 */
@Service
public class CheckUtil {

  @Autowired MainSession session;
  
  /**
   * Ellenőrzés: van nyitott fájl ?
   * @throws HiddenException Ha nincs nyitott fájl.
   */
  public void hasOpenedFile() throws HiddenException {
    
    if (!session.hasOpenedFile()) {
      throw new HiddenException("Nincs nyitott fájl!");
    }
    
  }
  
  /**
   * Ellenőrzés: van nyitott fájl, és aktuális ?
   * @throws HiddenException Ha nincs nyitott fájl, vagy nem aktuális.
   */
  public void isOpenedActual() throws HiddenException {
    
    if (!session.isOpenedActual()) {
      throw new HiddenException("Nincs nyitott fájl, vagy nem aktuális!");
    }
    
  }  
  
  /**
   * Ellenőrzés: van nyitott fájl, és tartalmazza a bejegyzés nevet ?
   * @throws HiddenException Ha nincs nyitott fájl, vagy nem tartalmazza a bejegyzés nevet.
   */
  public void hasOpenedNote(@NonNull String noteName) throws HiddenException {

    if (!session.hasOpenedNote(noteName)) {
      throw new HiddenException("Nincs nyitott fájl, vagy nem tartalmazza a bejegyzés nevet: " + noteName + "!");
    }
    
  }
  
  // ====
}
