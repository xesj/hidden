package xesj.app.util.main;
import xesj.app.system.MainApplication;
import xesj.app.system.MainSession;
import xesj.app.system.Property;

/**
 * Spring bean-ek lekérdezése
 */
public class SpBean {
  
  /**
   * MainApplication spring-bean lekérdezése
   */
  public static MainApplication application() {

    return MainApplication.context.getBean(MainApplication.class);
    
  } 

  /**
   * MainSession spring-bean lekérdezése
   */
  public static MainSession session() {

    return MainApplication.context.getBean(MainSession.class);
    
  } 

  /**
   * Property spring-bean lekérdezése
   */
  public static Property property() {

    return MainApplication.context.getBean(Property.class);
    
  } 

  /**
   * MainUtil spring-bean lekérdezése
   */
  public static MainUtil mainUtil() {

    return MainApplication.context.getBean(MainUtil.class);
    
  } 

  // ====
}
