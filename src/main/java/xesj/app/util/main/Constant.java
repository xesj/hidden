package xesj.app.util.main;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Állandó értékek
 */
public class Constant {

  /**
   * Redirect prefix
   */
  public static final String REDIRECT = "redirect:";
  
  /**
   * Ticket json-fájl neve
   */
  public static final String TICKET_JSON_NAME = "ticket.json";
  
  /**
   * LocalDate formátum: "éééé.hh.nn"
   */
  public static final DateTimeFormatter DATE_FORMAT_YMD = DateTimeFormatter.ofPattern("uuuu.MM.dd");

  /**
   * LocalDate formátum: "éééé.hh.nn óó:pp:mm"
   */
  public static final DateTimeFormatter DATE_FORMAT_YMD_HMS = DateTimeFormatter.ofPattern("uuuu.MM.dd HH:mm:ss");

  /**
   * Dátum formátum a hidden fájl nevében: "éééé-hh-nnTóó-pp-mm"
   */
  public static final DateTimeFormatter DATE_FORMAT_HIDDEN_FILE_NAME = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH-mm-ss");
  
  /**
   * Hidden fájl kezdő bájt értékek: "hidden" karakterek ascii kódjai.
   * Fájl mentéskor ezek az értékek íródnak a fájl első 6 bájtjába.
   */
  public static final byte[] FILE_START = {104, 105, 100, 100, 101, 110};
  
  /**
   * Hidden fájl megnyitásakor engedélyezett verziók. A fájl 7. bájtja.
   */
  public static final List<Integer> OPEN_FILE_VERSIONS = List.of(3);

  /**
   * Hidden fájl verziója mentéskor. Ez az érték íródik a fájl 7. bájtjába.
   */
  public static final int SAVE_FILE_VERSION = 3;
  
  /**
   * Megengedett karakterek a felhasználó által adott fájlnév elején
   */
  public static final String FILE_NAME_ENABLED_CHARACTERS = "abcdefghijklmnopqrstuvwxyz-0123456789";

  /** 
   * Hidden fájl név pattern. Formátum példa: titkos-adatok-1999-12-31T23-59-59.hidden
   */
  public static final Pattern FILE_NAME_PATTERN = Pattern.compile(
    "[" + FILE_NAME_ENABLED_CHARACTERS.replace("-", "\\-") + "]{1,64}-\\d{4}-\\d{2}-\\d{2}T\\d{2}-\\d{2}-\\d{2}\\.hidden"
  );
  
  /**
   * Ticket azonosító pattern. Formátum péda: 3657-9801-0154-1120-9878-2223-8530-7762
   */
  public static final Pattern TICKET_PATTERN = Pattern.compile("(\\d{4}-){7}\\d{4}");

  /**
   * Secret key algoritmus
   */
  public static final String SECRET_KEY_ALGORITHM = "AES";
  
  /**
   * Cipher algoritmus
   */
  public static final String CIPHER_ALGORITHM = "AES_256/GCM/NoPadding";
  
  /**
   * Az összes bejegyzés egy stringben való tárolásakor a bejegyzésNév-bejegyzésSzöveg párosainak elválasztó karaktere
   */
  public static final String NOTES_SEPARATOR = "\uFFFF"; 
  
  /**
   * Egy session-ben maximum ennyi jelszó lehet tárolva
   */
  public static final int MAXIMUM_PASSWORD_PER_SESSION = 16;
  
  /** 
   * Pattern: "{{" és "}}" közé zárt karaktersorozat melyben nem fordul elő sem a "{" sem a "}" karakter 
   */
  public static final Pattern LINK_PATTERN = Pattern.compile("\\{\\{[^\\{\\}]*\\}\\}");
  
  /**
   * Ez a kulcs tartalmazza az oldal címét a model-ben.
   */
  public static final String PAGE_TITLE = "pageTitle";

  /**
   * Figyelmeztetés akkor, ha a maximálisan megengedett értéket ekkora százalékban elérjük, vagy túllépjük.
   */
  public static final int WARNING_LIMIT = 80;
  
  /**
   * Salt0 bájt sorozat: 16 darab 0 értékű bájt.
   */
  public static final byte[] SALT0 = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  
  /**
   * Hidden-fájl baseName végződése, ha a titkosításához/visszafejtéséhez a salt0-t kell használni.
   */
  public static final String HIDDEN_FILE_BASENAME_END_SALT0 = "-salt0";
  
  // ====
}
