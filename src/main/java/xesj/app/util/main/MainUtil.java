package xesj.app.util.main;
import jakarta.servlet.http.HttpServletRequest;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import xesj.app.util.exception.BadSaltException;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationContext;
import xesj.tool.ByteTool;
import xesj.tool.LocaleTool;

/**
 * Fő utility
 */
@Service
public class MainUtil {
  
  @Autowired HttpServletRequest httpServletRequest;
  @Autowired @Lazy MessageSource messageSource;
  
  /**
   * Hivó IP-címének lekérdezése
   */
  public String getIP() {

    String ip = httpServletRequest.getHeader("X-Forwarded-For");
    if (ip == null) {
      ip = httpServletRequest.getRemoteAddr();
    }
    return ip;
    
  } 

  /**
   * FormValidationContext létrehozása
   */
  public FormValidationContext createFormValidationContext(
    BindingResult result, Integer fieldMessageMaximum, Integer globalMessageMaximum) {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    return new FormValidationContext(result, msl, fieldMessageMaximum, globalMessageMaximum);

  }

  /**
   * ValidationContext létrehozása 
   */
  public ValidationContext createValidationContext(
    Integer fieldMessageMaximum, Integer globalMessageMaximum, Integer throwCount) {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    return new ValidationContext(msl, fieldMessageMaximum, globalMessageMaximum, throwCount);

  }

  // ====
}
