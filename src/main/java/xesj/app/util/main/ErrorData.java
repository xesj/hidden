package xesj.app.util.main;
import java.util.Date;
import java.util.Map;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.error.ErrorAttributeOptions.Include;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.web.context.request.WebRequest;

/**
 * A keletkező exception adatait tartalmazó osztály, melyet a 3-as szintű exception handler használ fel.  
 */
public class ErrorData {
  
  public Throwable throwable;
  public Date timestamp;
  public Integer status;
  public String error;
  public String message;
  public String path;
  public String trace;
  public Map<String, Object> attributesMap;
  
  /**
   * Konstruktor
   */
  public ErrorData(ErrorAttributes errorAttributes, WebRequest webRequest) {
    
    // A szükséges objektumok megszerzése
    throwable = errorAttributes.getError(webRequest);
    ErrorAttributeOptions errorAttributeOptions = ErrorAttributeOptions.of(
      Include.MESSAGE, Include.STACK_TRACE
    );
    attributesMap = errorAttributes.getErrorAttributes(webRequest, errorAttributeOptions);

    // A példány feltöltése
    timestamp = (Date)attributesMap.get("timestamp");
    status = (Integer)attributesMap.get("status");
    error = (String)attributesMap.get("error");
    message = (String)attributesMap.get("message");
    path = (String)attributesMap.get("path");
    trace = (String)attributesMap.get("trace");
    
  }
  
  /**
   * A standard kimenetre kiírja az error-attribútum kulcsokat, és a hozzájuk tartozó érték típusát.
   * Csak tesztelési célokat szolgál.
   */
  public void print() {
    
    System.out.println("ErrorAttributes-map kulcsok, és az érték típusa:");
    for (String key: attributesMap.keySet()) {
      Object value = attributesMap.get(key);
      String valueType = null;
      if (value != null) {
        valueType = value.getClass().getName();
      }  
      System.out.println("  " + key + " -> " + valueType);
    }
    
  }
  
  // ====
}
