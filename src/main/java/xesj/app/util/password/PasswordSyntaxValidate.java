package xesj.app.util.password;
import xesj.app.util.exception.BadPasswordException;
import xesj.app.util.password.Password.Type;
import xesj.spring.validation.Message;
import xesj.spring.validation.validate.Validate;

/**
 * Jelszó szintaktikai validálása. 
 * Megfelel a formai követelményeknek ?
 */
public class PasswordSyntaxValidate extends Validate {
  
  /**
   * Konstruktor
   * @param text Validálandó jelszó. Null esetén nincs ellenőrzés.
   * @param type Jelszó típusa
   */
  public PasswordSyntaxValidate(String text, Type type) {
    
    // Password null ?
    if (text == null) return;
    
    //  A jelszó megfelel a formai követelményeknek ?
    try {
      new Password(text, type);
    }
    catch (BadPasswordException bpe) {
      message = new Message(null, null, bpe.getMessage());      
    }
    
  }
  
  // ====
}
