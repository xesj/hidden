package xesj.app.util.password;
import java.util.List;
import xesj.app.util.exception.BadPasswordException;
import xesj.app.util.exception.DecodeException;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.password.Password.Type;
import xesj.spring.validation.Message;
import xesj.spring.validation.validate.Validate;

/**
 * Jelszó validálása:
 *   1. Megfelel a formai követelményeknek ?
 *   2. Megnyitható vele legalább egy fájl ?
 */
public class PasswordValidate extends Validate {
  
  /**
   * Konstruktor
   * @param text 
   *        Validálandó jelszó. Null esetén nincs ellenőrzés.
   * @param type 
   *        Validálandó jelszó típusa. Null esetén nincs ellenőrzés.
   * @param fileName 
   *        Fájl neve path nélkül. 
   *        Null esetén a ticket-en belül az aktuális fájlok közül keres olyat mely dekódolható a jelszóval.
   *        Ha csak régi fájlok vannak, akkor azokon belül keres. 
   *        Null esetén az 'actual' paraméter figyelmen kívül marad. 
   * @param actual 
   *        Aktuális fájl ?
   */
  public PasswordValidate(String text, Type type, String fileName, boolean actual) {
    
    // Password null ?
    if (text == null) return;
    
    //  A jelszó megfelel a formai követelményeknek ?
    Password password;
    try {
      password = new Password(text, type);
    }
    catch (BadPasswordException bpe) {
      message = new Message(null, null, bpe.getMessage());      
      return;
    }
    
    // A fájlnév adott
    if (fileName != null) {
      try {
        HiddenFile hiddenFile = new HiddenFile(fileName, actual);
        hiddenFile.open(password);
      }
      catch (DecodeException de) {
        message = new Message(null, null, de.getMessage());
        return;
      }
      catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    
    // Nincs megadva fájlnév
    if (fileName == null) {
      List<HiddenFile> files = HiddenFile.getFiles(true, null);
      if (files.isEmpty()) {
        files = HiddenFile.getFiles(false, null);
      }
      for (HiddenFile hiddenFile: files) {
        try {
          hiddenFile.open(password);
          return; // Sikeres fájl dekódolás ezzel a jelszóval
        }
        catch (DecodeException de) {
        }
        catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
      message = new Message(null, null, "Ezzel a jelszóval egyetlen fájl sem nyitható meg!");
      return;
    }
    
  }
  
  // ====
}
