package xesj.app.util.password;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import lombok.Getter;
import lombok.NonNull;
import xesj.app.util.main.Constant;
import xesj.app.util.exception.BadPasswordException;
import xesj.app.util.main.SpBean;
import xesj.spring.html.Option;
import xesj.tool.ByteTool;

/**
 * Jelszó
 */
public class Password {
  
  /**
   * Jelszó típusa
   */
  public static enum Type {

    /**
     * Szöveges
     */
    TEXT,
    
    /**
     * Hexadecimális
     */
    HEXA;
    
  }
  
  /**
   * AES-256 kulcs általános hidden-fájlhoz
   */
  @Getter
  private SecretKey secretKey;
  
  /**
   * AES-256 kulcs "-salt0" végű hidden-fájlhoz
   */
  @Getter
  private SecretKey secretKeySalt0;
  
  /**
   * Konstruktor.
   * @param text A felhasználó által begépelt jelszó.
   * @param type A jelszó típusa.
   * @throws BadPasswordException Ha a jelszó nem felel meg a formai követelményeknek.
   */
  public Password(@NonNull String text, @NonNull Type type) throws BadPasswordException {
   
    // Előkészítés
    byte[] passwordBytes = null;

    // Szöveges jelszó
    if (type == Type.TEXT) {
      if (text.length() < 16) {
        throw new BadPasswordException("A szöveges jelszónak legalább 16 karakternek kell lennie!");
      }
      passwordBytes = text.getBytes(StandardCharsets.UTF_8);
    }

    // Hexadecimális jelszó
    if (type == Type.HEXA) {
      if (text.length() < 32) {
        throw new BadPasswordException("A hexadecimális jelszónak legalább 32 karakteresnek kell lennie!");
      }
      try {
        passwordBytes = ByteTool.hexToByteArray(text);
      }
      catch (NumberFormatException nfe) {
        throw new BadPasswordException("Hexadecimális jelszó hiba. " + nfe.getMessage());
      }
    }
    
    // secretKey előállítása
    secretKey = createSecretKey(passwordBytes, SpBean.application().getSaltHidden());

    // secretKeySalt0 előállítása
    secretKeySalt0 = createSecretKey(passwordBytes, Constant.SALT0);

  }
  
  /**
   * Secret key előállítása
   */
  private SecretKey createSecretKey(byte[] passwordBytes, byte[] salt) {
    
    // Kulcs előállítása
    byte[] byteArray = sha256(passwordBytes);
    for (int i = 0; i < salt.length; i++) {
      byteArray[(i * 13) % 32] = salt[i];
      byteArray = sha256(byteArray);
    }
    
    // Válasz
    return new SecretKeySpec(byteArray, Constant.SECRET_KEY_ALGORITHM);
    
  } 

  /**
   * SHA-256 hash készítése.
   * @param data Adat, melyből a hash készül.
   * @return SHA-256 hash.
   */
  private byte[] sha256(byte[] data) {

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      return md.digest(data);    
    }
    catch (NoSuchAlgorithmException nsae) {
      throw new RuntimeException(nsae);
    }

  }  
  
  /**
   * Jelszó típusok listája. 
   * View megjelenítéskor a jelszó típus választó radio gombokhoz szükséges. 
   */
  public static List<Option> getTypeOptions() {
    
    List<Option> passwordTypeOptions = new ArrayList<>();
    passwordTypeOptions.add(new Option(Password.Type.TEXT.toString(), "szöveges"));
    passwordTypeOptions.add(new Option(Password.Type.HEXA.toString(), "hexadecimális"));
    return passwordTypeOptions;
    
  }
  
  // ====
}
