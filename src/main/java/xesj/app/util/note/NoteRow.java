package xesj.app.util.note;

/**
 * Bejegyzés egy sora, a megjelenítési adatokkal együtt
 */
public class NoteRow {
  
  /** 
   * A bejegyzés egy sorának szövege 
   */
  public String text;
  
  /** 
   * A bejegyzés egy sorának szintje (balról való betolása) 
   */
  public int level;

  /** 
   * A bejegyzés egy sorának css osztálya 
   */
  public String css;
  
  // ====
}
