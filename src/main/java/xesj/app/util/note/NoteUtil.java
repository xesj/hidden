package xesj.app.util.note;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import org.springframework.stereotype.Service;
import xesj.app.util.main.Constant;
import xesj.tool.HtmlTool;

/**
 * Bejegyzés utility
 */
@Service
public class NoteUtil {
  
  /**
   * A bejegyzés szöveget előkészíti megjelenítéshez.
   * Sorokra darabolja, és beállítja a megjelenéséhez szükséges adatait.
   */
  public List<NoteRow> viewFormat(String text) {
    
    // Null ?
    if (text == null) {
      return null;
    }
    
    // Beállítások
    List<NoteRow> list = new ArrayList<>();
    String[] rows = text.split("\n", -1);
    int minimumLevel = Integer.MAX_VALUE;
    
    // Sorok feldolgozása
    for (String row: rows) {
      NoteRow noteRow = new NoteRow();
      
      // "=====>" sorkezdet
      if (row.startsWith("=====>")) {
        noteRow.text = row.substring(6);
        noteRow.level = 1;
      }
      
      // "----->" sorkezdet
      else if (row.startsWith("----->")) {
        noteRow.text = row.substring(6);
        noteRow.level = 2;
      }
      
      // Normál sorkezdet
      else {
        noteRow.text = row;
        noteRow.level = 3;
      }

      // "{{..}}" lecserélése html linkre, és a sor html escape-elése
      noteRow.text = replaceLinks(noteRow.text); 
      
      // Legkisebb level állítása
      if (noteRow.level < minimumLevel) {
        minimumLevel = noteRow.level;
      }
      
      // Hozzáadás a listához
      list.add(noteRow);
    }
    
    // css beállítása
    for (NoteRow noteRow: list) {
      StringBuilder css = new StringBuilder();
      // Minden sorra
      css.append("note-row fs-5 ");
      // Háttér, szöveg, padding, margó
      if (noteRow.level == 1) css.append("bg-primary opacity-50 text-white px-2 my-2 ");
      if (noteRow.level == 2) css.append("bg-success opacity-50 text-white px-2 my-2 ");
      if (noteRow.level == 3) css.append("text-secondary ");
      // Betolás
      int betolas = noteRow.level - minimumLevel;
      if (betolas == 1) css.append("ms-4");
      if (betolas == 2) css.append("ms-5");
      // Végső css      
      noteRow.css = css.toString();
    }

    // Válasz
    return list;
    
  }
  
  /**
   * A stringben lévő összes {{...}} lecserélése html linkre, és a string html escape-elése
   */
  private String replaceLinks(String str) {
    
    Matcher matcher = Constant.LINK_PATTERN.matcher(str);
    StringBuilder builder = new StringBuilder();
    int copy = 0;
    while (matcher.find()) {
      int start = matcher.start();
      int end = matcher.end();
      builder.append(HtmlTool.escape(str.substring(copy, start)));
      builder.append(replaceOneLink(str.substring(start, end)));
      copy = end;
    }
    builder.append(HtmlTool.escape(str.substring(copy)));
    return builder.toString();
    
  }
  
  /**
   * Egyetlen {{URL}} lecserélése html linkre: <a href="URL" target="_blank">URL</a>
   * @param str Átalakítandó string mely mindig "{{...}}" formátumú
   */
  private String replaceOneLink(String str) {
    
    // {{...}} belüli rész kiemelése, trim-melése, és html escape-elése
    String url = HtmlTool.escape(str.substring(2, str.length() - 2).trim());
    
    // Meghatározzuk a link összetevőit: href, text, target
    String href = url;
    String text = url;
    boolean targetBlank = true;
    if (url.startsWith("mailto:")) {
      // Email esetén a "mailto:" kezdet levágása
      href = "mailto:" + url.substring(7).trim();
      text = url.substring(7).trim();
      targetBlank = false;
    }
    
    // Link összeállítása 
    StringBuilder builder = new StringBuilder();
    builder
      .append("<a")
      .append(" href=\"").append(href).append("\"");
    if (targetBlank) {
      builder.append(" target=\"_blank\"");
    }  
    builder
      .append(">")
      .append(text)
      .append("</a>");
    return builder.toString();

  }
  
  /**
   * Bejegyzés szövegének trim-melése úgy, hogy ne legyenek benne üres kezdő és záró sorok.
   * @param noteText Bejegyzés szövege.
   */
  public String trim(String noteText) {
    
    // Nincs bejegyzés ?
    if (noteText == null) return null;
    
    // Szöveg darabolása a sorváltások mentén
    var array = noteText.split("\n", -1);
    var list = new ArrayList<>(Arrays.asList(array));

    // Üres záró sorok törlése a listából
    for (int i = list.size() - 1; i >= 0; i--) {
      if (!list.get(i).trim().isEmpty()) break;
      list.remove(i);
    }

    // Üres kezdő sorok törlése a listából
    while (!list.isEmpty() && list.get(0).trim().isEmpty()) {
      list.remove(0);
    }
    
    // Válasz
    return (list.isEmpty() ? null : String.join("\n", list));

  }

  // ====
}
