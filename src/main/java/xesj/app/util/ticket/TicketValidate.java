package xesj.app.util.ticket;
import xesj.app.util.exception.BadTicketException;
import xesj.spring.validation.Message;
import xesj.spring.validation.validate.Validate;

/**
 * Ticket validálása
 */
public class TicketValidate extends Validate {

  /**
   * Konstruktor
   * @param id Ticket azonosító.
   */
  public TicketValidate(String id) {
    
    // Van adat ?
    if (id == null) return;
    
    // Ellenőrzés
    try {
      new Ticket(id); 
    }
    catch (BadTicketException bte) {
      message = new Message(null, null, bte.getMessage());
      return;
    }
    
  }

  // ====
}
