package xesj.app.util.ticket;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.regex.Matcher;
import lombok.Getter;
import lombok.NonNull;
import xesj.app.util.main.Constant;
import xesj.app.util.exception.BadTicketException;
import xesj.app.util.exception.HiddenException;
import xesj.app.util.main.SpBean;
import xesj.json.JsonNodeReader;
import xesj.tool.StringTool;

/**
 * Ticket
 */
@Getter
public class Ticket {
  
  /**
   * Ticket azonosító. Nem lehet null!
   */
  private String id;

  /**
   * Ticket megnevezése, kinek, miért lett kiosztva. 
   * Nem lehet null!
   */
  private String name;

  /** 
   * Egy hidden fájl méretének maximuma string-ként megadva, például: "100K", "4M", "1G". 
   * Az egész érték >= 1 lehet. Az egész érték után kötelező a betűjel, 
   * mely "K" (kilobájt), "M" (megabájt), vagy "G" (gigabájt) lehet.
   * Nem lehet null!
   */
  private String size;

  /** 
   * Egy hidden fájl méretének maximuma bájtokban megadva. 
   * A "size" alapján számított mező, ezért nem tárolódik a ticket.json fájlban.
   * Nem lehet null!
   */
  private long sizeBytes;
  
  /**
   * Maximum ennyi darab (aktuális + régi) hidden fájl lehet összesen. 
   * Értéke >= 1 lehet. 
   * Nem lehet null!
   */
  private int unit; 
  
  /**
   * Konstruktor. 
   * Csak akkor jön létre a példány, ha a ticket a fájlrendszerben is létezik helyes beállításokkal.
   * @param id Ticket azonosító.
   * @throws BadTicketException 
   *         Ha a ticket azonosító formátuma hibás, vagy nem létezik a fájlrendszerben.
   * @throws HiddenException 
   *         A fájlrendszer kialakításának technikai hibája.
   */
  public Ticket(@NonNull String id) throws BadTicketException {
    
    // ID példányváltozó feltöltése
    this.id = id;
    
    // Formai ellenőrzés
    Matcher matcher = Constant.TICKET_PATTERN.matcher(id);
    if (!matcher.matches()) {
      throw new BadTicketException("A meghívó formátuma nem megfelelő!");
    }
    
    // Létezik a fájlrendszerben a ticket könyvtár ?
    if (!existDirectory()) {
      throw new BadTicketException("Érvénytelen meghívó!");
    }  

    // Létezik a fájlrendszerben a ticket könyvtár alatt "old" könyvtár ?
    if (!existOldDirectory()) {
      throw new HiddenException("Nem létezik 'old' alkönyvtár!");
    }  

    // Létezik a fájlrendszerben a ticket.json fájl ?
    if (!existJson()) {
      throw new HiddenException("Nem létezik '" + Constant.TICKET_JSON_NAME + "' fájl!");
    }  
    
    // ticket.json fájl beolvasás JsonNode-ba
    JsonNodeReader jsonNodeReader;
    try {
      Path path = Paths.get(SpBean.property().getBaseDir(), id, Constant.TICKET_JSON_NAME);
      JsonNode jsonNode = new ObjectMapper().readValue(path.toFile(), JsonNode.class);
      jsonNodeReader = new JsonNodeReader(jsonNode);
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }

    // Adatok kiszedése JsonNode-ból
    String jsonId = jsonNodeReader.getString("id");
    String jsonName = jsonNodeReader.getString("name");
    String jsonSize = jsonNodeReader.getString("size");
    Integer jsonUnit = jsonNodeReader.getInteger("unit");  

    // jsonId ellenőrzése
    if (jsonId == null || !jsonId.equals(id)) {
      throw new HiddenException(Constant.TICKET_JSON_NAME + " (id) technikai hiba!");
    }
    
    // jsonName ellenőrzése
    if (StringTool.isNullOrEmpty(jsonName)) {
      throw new HiddenException(Constant.TICKET_JSON_NAME + " (name) technikai hiba!");
    }
    this.name = jsonName;
    
    // jsonUnit ellenőrzése
    if (jsonUnit == null || jsonUnit < 1) {
      throw new HiddenException(Constant.TICKET_JSON_NAME + " (unit) technikai hiba!");
    }
    this.unit = jsonUnit;
    
    // jsonSize ellenőrzése
    final String SIZE_ERROR_TEXT = Constant.TICKET_JSON_NAME + " (size) technikai hiba!"; 
    if (jsonSize == null || jsonSize.length() < 2) {
      throw new HiddenException(SIZE_ERROR_TEXT);
    }
    char sizeKMG = jsonSize.charAt(jsonSize.length() - 1);
    if (sizeKMG != 'K' && sizeKMG != 'M' && sizeKMG != 'G') {
      throw new HiddenException(SIZE_ERROR_TEXT);
    }
    int sizeValue;
    try {
      sizeValue = Integer.parseInt(jsonSize.substring(0, jsonSize.length() - 1));
    }
    catch (NumberFormatException nfe) {
      throw new HiddenException(SIZE_ERROR_TEXT);
    }
    if (sizeValue < 1) {
      throw new HiddenException(SIZE_ERROR_TEXT);
    }
    this.size = jsonSize;
    
    // sizeBytes kiszámítása
    sizeBytes = switch (sizeKMG) {
      case 'K' -> 1024L * sizeValue;
      case 'M' -> 1024L * 1024L * sizeValue;
      case 'G' -> 1024L * 1024L * 1024L * sizeValue;
      default -> 0; // Ez az eset nem lehetséges 
    };
    
  }
  
  /**
   * Létezik a ticket könyvtár a fájlrendszerben ?
   */
  private boolean existDirectory() {
    
     Path path = Paths.get(SpBean.property().getBaseDir(), id);
     return path.toFile().exists();
    
  }
  
  /**
   * Létezik a ticket könyvtár alatt "old" könyvtár a fájlrendszerben ?
   */
  private boolean existOldDirectory() {
    
     Path path = Paths.get(SpBean.property().getBaseDir(), id, "old");
     return path.toFile().exists();
    
  }

  /**
   * Létezik a ticket könyvtár alatt a ticket.json fájl ?
   */
  private boolean existJson() {
    
     Path path = Paths.get(SpBean.property().getBaseDir(), id, Constant.TICKET_JSON_NAME);
     return path.toFile().exists();
    
  }

  /**
   * Egyenlőség vizsgálata. Akkor egyenlő két ticket, ha megegyezik az azonosítójuk.
   */
  @Override
  public boolean equals(Object object) {

    if (!(object instanceof Ticket)) {
      return false;
    }
    
    Ticket ticket2 = (Ticket)object;
    return id.equals(ticket2.id);

  }
  
  /**
   * Csak az equals() megvalósítás miatt szükséges.   
  */
  @Override
  public int hashCode() {

    return Objects.hashCode(id);

  }
  
  // ====
}
