package xesj.app.test;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.app.util.exception.ConcurrentException;
import xesj.app.util.exception.FileSizeException;
import xesj.app.util.exception.HiddenException;
import xesj.app.util.exception.UnitException;
import xesj.tool.StringTool;

/**
 * Teszt controller a program fejlesztéséhez
 */
@Controller
public class TestController {
  
  public static final String PATH = "/test";
  private static final String VIEW = "/test/test.html";
  
  /**
   * GET: /test
   */
  @GetMapping(PATH)
  public String get(@RequestParam(required = false) String exception) {
    
    // HiddenException kiváltása 
    if (StringTool.equal(exception, "hidden")) {
      throw new HiddenException("HiddenException szövege!");
    }

    // ArithmeticException kiváltása 
    if (StringTool.equal(exception, "arithmetic")) {
      System.out.println(1/0);
    }

    // UnitException kiváltása 
    if (StringTool.equal(exception, "unit")) {
      throw new UnitException(120, 345);
    }

    // FileSizeException kiváltása 
    if (StringTool.equal(exception, "filesize")) {
      throw new FileSizeException(48000000);
    }

    // ConcurrentException kiváltása 
    if (StringTool.equal(exception, "concurrent")) {
      throw new ConcurrentException("ConcurrentException szövege!");
    }

    // View megjelenítése
    return VIEW;

  }
  
  // ====
}
