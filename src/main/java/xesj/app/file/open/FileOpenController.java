package xesj.app.file.open;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.app.note.list.NoteListController;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.exception.DecodeException;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.main.MainUtil;
import xesj.app.util.password.Password;
import xesj.app.util.password.PasswordValidate;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.RequiredValidate;
import xesj.spring.validation.validate.SetValidate;

/**
 * Hidden fájl megnyitása controller
 */
@Controller
public class FileOpenController {
  
  public static final String PATH = "/file/open";
  private static final String VIEW = "/file/file-open.html";

  @Autowired MainSession session;
  @Autowired MainUtil mainUtil;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Fájl megnyitása");
    model.addAttribute("typeOptions", Password.getTypeOptions());
    
  }
  
  /**
   * GET: /file/open
   */
  @GetMapping(PATH)
  public String get(@RequestParam String file, @RequestParam boolean actual, Model model) throws Exception {
    
    // HiddenFile előállítása
    HiddenFile hiddenFile = new HiddenFile(file, actual);
    
    // A fájl már nyitva van (dekódolva van) a session-ben ?
    if (hiddenFile.isOpened()) {
      // Redirect a bejegyzések listájára
      return Constant.REDIRECT + NoteListController.PATH;
    }  
    
    // A fájl még nincs nyitva, de megpróbáljuk megnyitni a session-ben eddig tárolt jelszavakkal 
    for (Password password: session.getPasswords()) {
      try {
        hiddenFile.open(password);
        // Sikeres dekódolás, fájl tárolása a session-ben
        session.setOpenedFile(hiddenFile);
        // Redirect a bejegyzések listájára
        return Constant.REDIRECT + NoteListController.PATH;
      }
      catch (DecodeException de) {
        // Sikertelen dekódolás
      }
    }
    
    // Form beállítása
    FileOpenForm form = new FileOpenForm();
    form.setFile(file);
    form.setActual(actual);
    form.setTipus(Password.Type.TEXT.name());
    
    // Model beállítása
    model.addAttribute("form", form);
    model.addAttribute("hiddenFile", hiddenFile);

    // Jelszó bekérő view megjelenítése
    return VIEW;

  }
  
  /**
   * POST: /file/open
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") FileOpenForm form, BindingResult result, Model model) throws Exception {
    
    // Validáció
    validate(form, result);

    // HiddenFile előállítása
    HiddenFile hiddenFile = new HiddenFile(form.getFile(), form.isActual());
    
    // Validációs hiba esetén saját view megjelenítése
    if (result.hasErrors()) {
      model.addAttribute("hiddenFile", hiddenFile);
      return VIEW;
    }
    
    // Password objektum előállítása
    Password password = new Password(form.getJelszo(), Password.Type.valueOf(form.getTipus()));

    // Password bejegyzése a password-listába
    session.add(password, false);
    
    // HiddenFile dekódolás
    hiddenFile.open(password);
    
    // HiddenFile tárolása a session-ben
    session.setOpenedFile(hiddenFile);
    
    // Redirect a bejegyzések listájára
    return Constant.REDIRECT + NoteListController.PATH;
    
  }
  
  /**
   * Validáció
   */
  private void validate(FileOpenForm form, BindingResult result) {
    
    // Előkészítés
    FormValidationContext context = mainUtil.createFormValidationContext(result, 1, null);
    String field;
    
    // Tipus
    field = "tipus";
    context.add(field, 
      () -> new RequiredValidate(form.getTipus()),
      () -> new SetValidate(form.getTipus(), Password.Type.TEXT.name(), Password.Type.HEXA.name())
    );

    // Jelszo
    field = "jelszo";
    context.add(field, 
      () -> new RequiredValidate(form.getJelszo(), false),
      () -> new LengthValidate(form.getJelszo(), 1L, 64L)
    );
    
    // Megnyitható a jelszóval a fájl ? 
    if (!result.hasErrors()) {
      context.add(field, 
        () -> new PasswordValidate(
          form.getJelszo(), 
          Password.Type.valueOf(form.getTipus()), 
          form.getFile(), 
          form.isActual()
        )
      );
    }
    
  }
  
  // ====
}
