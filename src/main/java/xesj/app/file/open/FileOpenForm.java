package xesj.app.file.open;
import lombok.Getter;
import lombok.Setter;

/**
 * Hidden fájl megnyitása form
 */
@Getter
@Setter
public class FileOpenForm {
  
  /**
   * Fájlnév
   */
  private String file;

  /**
   * Aktuális fájl ?
   */
  private boolean actual;

  /**
   * Jelszó típusa. "TEXT" = szöveges, "HEXA" = hexadecimális 
   */
  private String tipus; 
  
  /**
   * Jelszó
   */
  private String jelszo;  

}
