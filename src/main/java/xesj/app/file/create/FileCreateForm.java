package xesj.app.file.create;
import lombok.Getter;
import lombok.Setter;

/**
 * Hidden fájl létrehozása form
 */
@Getter
@Setter
public class FileCreateForm {
  
  /**
   * Fájlnév kezdet
   */
  private String fajlnevKezdet;

  /**
   * Jelszó típusa. "TEXT" = szöveges, "HEXA" = hexadecimális 
   */
  private String jelszoTipus; 
  
  /**
   * Jelszó
   */
  private String jelszo;  

  /**
   * Jelszó megismétlése
   */
  private String jelszo2;  

}
