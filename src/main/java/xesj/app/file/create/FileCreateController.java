package xesj.app.file.create;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import xesj.app.note.list.NoteListController;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.main.MainUtil;
import xesj.app.util.password.Password;
import xesj.app.util.password.PasswordSyntaxValidate;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.validate.CharacterContentValidate;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.RequiredValidate;

/**
 * Hidden fájl létrehozása controller
 */
@Controller
public class FileCreateController {

  public static final String PATH = "/file/create";
  private static final String VIEW = "/file/file-create.html";
  
  @Autowired MainSession session;
  @Autowired MainUtil mainUtil;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Fájl létrehozása");
    model.addAttribute("typeOptions", Password.getTypeOptions());
    
  }
  
  /**
   * GET: /file/create
   */
  @GetMapping(PATH)
  public String get(Model model) throws Exception {
    
    // Form beállítása
    FileCreateForm form = new FileCreateForm();
    form.setJelszoTipus(Password.Type.TEXT.name());
    
    // Model beállítása
    model.addAttribute("form", form);

    // View megjelenítése
    return VIEW;

  }
  
  /**
   * POST: /file/create
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") FileCreateForm form, BindingResult result, Model model) throws Exception {
    
    // Validáció
    validate(form, result);
    
    // Validációs hiba esetén saját view megjelenítése
    if (result.hasErrors()) {
      return VIEW;
    }

    // HiddenFile létrehozása
    HiddenFile hiddenFile = new HiddenFile(
      form.getFajlnevKezdet(),
      form.getJelszo(), 
      Password.Type.valueOf(form.getJelszoTipus())      
    );

    // HiddenFile mentés a fájlrendszerbe
    hiddenFile.save();

    // Password bejegyzése a password-listába
    session.add(hiddenFile.getPassword(), false);
    
    // HiddenFile tárolása a session-ben, ez lesz a megnyitott fájl
    session.setOpenedFile(hiddenFile);
    
    // Redirect a bejegyzések listájára
    return Constant.REDIRECT + NoteListController.PATH;
    
  }
  
  /**
   * Validáció
   */
  private void validate(FileCreateForm form, BindingResult result) {
    
    // Előkészítés
    FormValidationContext context = mainUtil.createFormValidationContext(result, 1, null);
    String field;
    
    // fajlnevKezdet
    field = "fajlnevKezdet";
    context.add(field, 
      () -> new RequiredValidate(form.getFajlnevKezdet()),
      () -> new LengthValidate(form.getFajlnevKezdet(), 1L, 64L),
      () -> new CharacterContentValidate(form.getFajlnevKezdet(), Constant.FILE_NAME_ENABLED_CHARACTERS)
    );
    
    // Ez a fájlnév kezdet létezik az aktuális könyvtárban ?
    if (context.enableAddMessage(field)) {
      List<HiddenFile> hiddenFiles = HiddenFile.getFiles(true, null);
      for (HiddenFile hiddenFile: hiddenFiles) {
        if (form.getFajlnevKezdet().equals(hiddenFile.getBaseName())) {
          context.add(field, "Ilyen nevű fájl már létezik!");
        }
      }  
    }
    
    // jelszoTipus
    field = "jelszoTipus";
    context.add(field, 
      () -> new RequiredValidate(form.getJelszoTipus())
    );

    // jelszo
    field = "jelszo";
    context.add(field, 
      () -> new RequiredValidate(form.getJelszo()),
      () -> new LengthValidate(form.getJelszo(), 1L, 64L),
      () -> new PasswordSyntaxValidate(form.getJelszo(), Password.Type.valueOf(form.getJelszoTipus()))
    );

    // jelszo2
    field = "jelszo2";
    context.add(field, 
      () -> new RequiredValidate(form.getJelszo2()),
      () -> new LengthValidate(form.getJelszo2(), 1L, 64L),
      () -> new PasswordSyntaxValidate(form.getJelszo2(), Password.Type.valueOf(form.getJelszoTipus()))
    );
    
    // Egyezik a két jelszó ?
    if (!result.hasFieldErrors("jelszo") && !result.hasFieldErrors("jelszo2")) {
      if (!form.getJelszo().equals(form.getJelszo2())) {
        context.add(null, "Nem egyezik a két jelszó!");
      }
    }
    
  }
  
  // ====
}
