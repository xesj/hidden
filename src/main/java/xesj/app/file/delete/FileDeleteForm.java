package xesj.app.file.delete;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * Hidden fájl törlése form
 */
@Getter
@Setter
public class FileDeleteForm {
  
  /**
   * Keresés: szótöredék 
   */
  private String fragment;

  /**
   * Keresés: aktuális fájlok megjelenítése 
   */
  private boolean actual;
  
  /**
   * Lenyomott submit gomb. 
   */
  private String submit;
  
  /**
   * Törlendő hidden fájlok (bejelölt checkbox-ok). Formátum: "{fájlnév}|{aktuális?}"
   */
  private List<String> tobedeleted;

}
