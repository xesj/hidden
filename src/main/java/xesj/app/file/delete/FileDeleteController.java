package xesj.app.file.delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;
import xesj.tool.StringTool;

/**
 * Hidden fájl törlése controller
 */
@Controller
public class FileDeleteController {
  
  public static final String PATH = "/file/delete";
  private static final String VIEW = "/file/file-delete.html";

  @Autowired MainSession session;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Fájlok törlése");
    
  }
  
  /**
   * GET: /file/delete
   */
  @GetMapping(PATH)
  public String get(Model model) {
    
    // Form beállítása
    FileDeleteForm form = new FileDeleteForm();
    model.addAttribute("form", form);
    
    // Fájlok listája
    getHiddenFiles(form, model);

    // View megjelenítése
    return VIEW;
  }
  
  /**
   * POST: /file/delete
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") FileDeleteForm form, BindingResult result, Model model) {
    
    // Fájltörlés gomb esetén: fájlok törlése a fájlrendszerből
    if (StringTool.equal(form.getSubmit(), "FAJLTORLES")) {
      if (form.getTobedeleted() != null) {
        for (String tobedeleted: form.getTobedeleted()) {
          int pos = tobedeleted.indexOf('|');
          String name = tobedeleted.substring(0, pos);
          boolean actual = Boolean.parseBoolean(tobedeleted.substring(pos + 1));
          HiddenFile hiddenFile = new HiddenFile(name, actual);
          // A törlendő fájl van éppen megnyitva ?
          if (session.hasOpenedFile()) {
            if (session.getOpenedFile().equals(hiddenFile)) {
              session.setOpenedFile(null);
            }
          }
          // A fájl fizikai törlése
          hiddenFile.deleteFile();
        }
      }  
    }
    
    // Törlés gomb esetén: feltételek törlése
    if (StringTool.equal(form.getSubmit(), "TORLES")) {
      form.setFragment(null);
      form.setActual(false);
    }
    
    // Fájlok listája
    getHiddenFiles(form, model);

    // View megjelenítése
    return VIEW;
    
  }
  
  /**
   * Fájlok listájának összeállítása, tárolása a model-be
   */
  private void getHiddenFiles(FileDeleteForm form, Model model) {

    // Régi fájlok
    model.addAttribute(
      "oldFiles", 
      HiddenFile.getFiles(false, form.getFragment())
    );
    
    // Aktuális fájlok (ha szükséges)
    if (form.isActual()) {
      model.addAttribute(
        "actualFiles", 
        HiddenFile.getFiles(true, form.getFragment())
      );
    }  
    
  }
  
  // ====
}
