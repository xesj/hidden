package xesj.app.file.warning;

/**
 * Fájl méretének részletezése
 */
public class SizeDetail {
  
  /**
   * Fájl alapnév
   */
  public String baseName;
  
  /**
   * Százalékban fejezi ki, hogy a fájl a megengedett maximális mérethez képest mekkora. 
   */
  public long percent;
  
  /**
   * Konstruktor
   */
  public SizeDetail(String baseName, long percent) {

    this.baseName = baseName;
    this.percent = percent;

  }
  
  // ====
}
