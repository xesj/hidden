package xesj.app.file.warning;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.app.file.list.FileListController;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.ticket.Ticket;

/**
 * Felhasználónak szóló figyelmeztetések controller  
 */
@Controller
public class FileWarningController {
  
  public static final String PATH = "/file/warning";
  private static final String VIEW = "/file/file-warning.html";

  @Autowired MainSession session;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Figyelmeztetések");
    
  }
  
  /**
   * GET: /file/warning
   * @param visible 
   *        True: mindenképp meg kell jelennie ennek az oldalnak.
   *        False: csak akkor kell megjelennie az oldalnak ha van figyelmeztetés a felhasználó számára.
   *               Ha nincs figyelmeztetés akkor a fájlok listája oldalra kell redirect-álni.
   */
  @GetMapping(PATH)
  public String get(@RequestParam boolean visible, Model model) {
    
    // Warning limit
    model.addAttribute("warningLimit", Constant.WARNING_LIMIT);
    
    // Ticket információk 
    Ticket ticket = session.getTicket();
    model.addAttribute("ticketSizeByte", ticket.getSizeBytes());
    model.addAttribute("ticketSizeText", ticket.getSize());
    model.addAttribute("ticketUnit", ticket.getUnit());
    
    // Fájl információk
    List<HiddenFile> actualFiles = HiddenFile.getFiles(true, null);
    List<HiddenFile> oldFiles = HiddenFile.getFiles(false, null);
    model.addAttribute("unit", actualFiles.size() + oldFiles.size());
    model.addAttribute("unitActual", actualFiles.size());
    model.addAttribute("unitOld", oldFiles.size());
    boolean unitWarning = actualFiles.size() + oldFiles.size() >= ticket.getUnit() * Constant.WARNING_LIMIT / 100.0;
    model.addAttribute("unitWarning", unitWarning);

    // Aktuális fájlok méretének ellenőrzése
    List<SizeDetail> sizeFiles = new ArrayList<>(); 
    for (HiddenFile hiddenFile: actualFiles) {
      long hiddenSize = hiddenFile.getFile().length();
      if (hiddenSize >= ticket.getSizeBytes() * Constant.WARNING_LIMIT / 100.0) {
        long percent = 100 * hiddenSize / ticket.getSizeBytes(); 
        SizeDetail sizeDetail = new SizeDetail(hiddenFile.getBaseName(), percent);
        sizeFiles.add(sizeDetail);
      }  
    }
    model.addAttribute("sizeFiles", sizeFiles);
    boolean sizeWarning = !sizeFiles.isEmpty();
    model.addAttribute("sizeWarning", sizeWarning);

    // Saját view megjelenítése, vagy redirect a fájlok listájára
    return (visible || unitWarning || sizeWarning ? VIEW : Constant.REDIRECT + FileListController.PATH);

  }
  
  // ====
}
