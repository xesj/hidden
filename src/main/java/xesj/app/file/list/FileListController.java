package xesj.app.file.list;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;
import xesj.tool.StringTool;

/**
 * Hidden fájlok listázása megnyitás céljából controller
 */
@Controller
public class FileListController {
  
  public static final String PATH = "/file/list";
  private static final String VIEW = "/file/file-list.html";

  @Autowired MainSession session;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Fájlok");
    
  }
  
  /**
   * GET: /file/list
   */
  @GetMapping(PATH)
  public String get(Model model) {
    
    // Form beállítása
    FileListForm form = Optional.ofNullable(session.getFileListForm()).orElse(new FileListForm());
    model.addAttribute("form", form);
    
    // Fájlok listája
    getHiddenFiles(form, model);

    // View megjelenítése
    return VIEW;
    
  }
  
  /**
   * POST: /file/list
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") FileListForm form, BindingResult result, Model model) {
    
    // Törlés gomb esetén: feltételek törlése
    if (StringTool.equal(form.getSubmit(), "TORLES")) {
      form.setFragment(null);
      form.setOld(false);
    }

    // Form tárolása a session-ben
    session.setFileListForm(form);
    
    // Fájlok listája
    getHiddenFiles(form, model);

    // View megjelenítése
    return VIEW;
    
  }
  
  /**
   * Fájlok listájának összeállítása, tárolása a model-be
   */
  private void getHiddenFiles(FileListForm form, Model model) {
    
    // Aktuális fájlok
    model.addAttribute(
      "actualFiles", 
      HiddenFile.getFiles(true, form.getFragment())
    );

    // Régi fájlok (ha szükséges)
    if (form.isOld()) {
      model.addAttribute(
        "oldFiles", 
        HiddenFile.getFiles(false, form.getFragment())
      );
    }  
    
  }
  
  // ====
}
