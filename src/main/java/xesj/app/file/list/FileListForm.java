package xesj.app.file.list;
import lombok.Getter;
import lombok.Setter;

/**
 * Hidden fájlok listázása megnyitás céljából form
 */
@Getter
@Setter
public class FileListForm {
  
  private String fragment;
  private boolean old;
  private String submit;

}
