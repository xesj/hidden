package xesj.app.file.download;
import jakarta.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;
import xesj.tool.StreamTool;

/**
 * Az aktuális és régi hidden fájlok ZIP-be csomagolása, és letöltése controller
 */
@Controller
public class FileDownloadController {
  
  public static final String PATH = "/file/download";

  @Autowired MainSession session;
  
  /**
   * GET: /file/download
   */
  @GetMapping(PATH)
  public void get(HttpServletResponse response) throws FileNotFoundException, IOException {

    // Előkészítés
    ZipEntry entry;
    
    // ZIP fájl nevének előállítása
    String zipName = 
      "hidden-files-backup-ticket" + 
      session.getTicket().getId().substring(0, 4) + "-" +
      LocalDateTime.now().format(Constant.DATE_FORMAT_HIDDEN_FILE_NAME) + 
      ".zip";

    // Fájlok listája
    List<HiddenFile> hiddenFiles = HiddenFile.getFiles(true, null);
    hiddenFiles.addAll(HiddenFile.getFiles(false, null));
    
    // Írás a kimenetre
    response.setContentType("application/zip");
    response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
    try (OutputStream os = response.getOutputStream(); ZipOutputStream zos = new ZipOutputStream(os)) {

      // "old" könyvtárnév írása zip-be
      entry = new ZipEntry("old/");
      zos.putNextEntry(entry);
      zos.closeEntry();      
      
      // Ciklus a hidden fájlokon 
      for (HiddenFile hiddenFile: hiddenFiles) {
        
        // Entry nyitás
        entry = new ZipEntry(
          hiddenFile.isActual() ? hiddenFile.getName() : "old/" + hiddenFile.getName()
        );
        zos.putNextEntry(entry);
        
        // Bájtsorozat másolás
        StreamTool.copy(new FileInputStream(hiddenFile.getFile()), zos, null);
        
        // Entry zárás
        zos.closeEntry();
      }
    }  

  }
  
  // ====
}
