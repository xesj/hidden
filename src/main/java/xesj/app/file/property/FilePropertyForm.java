package xesj.app.file.property;
import lombok.Getter;
import lombok.Setter;

/**
 * Fájlnév, jelszó módosítása form 
 */
@Getter
@Setter
public class FilePropertyForm {
  
  /**
   * Fájlnév kezdet
   */
  private String fajlnevKezdet;

  /**
   * Jelszó típusa. "TEXT" = szöveges, "HEXA" = hexadecimális 
   */
  private String jelszoTipus; 
  
  /**
   * Jelszó
   */
  private String jelszo;  

  /**
   * Jelszó megismétlése
   */
  private String jelszo2;  

}
