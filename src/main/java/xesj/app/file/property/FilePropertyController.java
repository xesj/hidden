package xesj.app.file.property;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import xesj.app.note.list.NoteListController;
import xesj.app.system.MainSession;
import xesj.app.util.main.CheckUtil;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.main.MainUtil;
import xesj.app.util.password.Password;
import xesj.app.util.password.PasswordSyntaxValidate;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.validate.CharacterContentValidate;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.RequiredValidate;
import xesj.tool.StringTool;

/**
 * Fájlnév, jelszó módosítása controller 
 */
@Controller
public class FilePropertyController {

  public static final String PATH = "/file/property";
  private static final String VIEW = "/file/file-property.html";
  
  @Autowired MainSession session;
  @Autowired MainUtil mainUtil;
  @Autowired CheckUtil checkUtil;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Fájlnév, jelszó módosítása");
    model.addAttribute("typeOptions", Password.getTypeOptions());
    
  }
  
  /**
   * GET: /file/property
   */
  @GetMapping(PATH)
  public String get(Model model) throws Exception {
    
    // Ellenőrzés
    check();
    
    // Form beállítása
    FilePropertyForm form = new FilePropertyForm();
    form.setJelszoTipus(Password.Type.TEXT.name());
    
    // Model beállítása
    model.addAttribute("form", form);

    // View megjelenítése
    return VIEW;

  }
  
  /**
   * POST: /file/property
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") FilePropertyForm form, BindingResult result, Model model) throws Exception {

    // Ellenőrzés
    check();
    
    // Validáció
    validate(form, result);
    
    // Validációs hiba esetén saját view megjelenítése
    if (result.hasErrors()) {
      return VIEW;
    }
    
    // Hidden fájl 
    HiddenFile hiddenFile = session.getOpenedFile();
    
    // Fájl információk tárolása, az "old" könyvtárba mozgatáshoz
    hiddenFile.oldInfo();
    
    // Property módosítás
    hiddenFile.property(
      form.getFajlnevKezdet(),
      form.getJelszo(), 
      Password.Type.valueOf(form.getJelszoTipus())      
    );
    
    // HiddenFile mentés a fájlrendszerbe
    hiddenFile.save();

    // Password bejegyzése a password-listába
    session.add(hiddenFile.getPassword(), false);
    
    // Redirect a bejegyzések listájára
    return Constant.REDIRECT + NoteListController.PATH;
    
  }
  
  /**
   * Validáció
   */
  private void validate(FilePropertyForm form, BindingResult result) {
    
    // Előkészítés
    FormValidationContext context = mainUtil.createFormValidationContext(result, 1, null);
    String field;
    
    // fajlnevKezdet
    field = "fajlnevKezdet";
    context.add(field, 
      () -> new LengthValidate(form.getFajlnevKezdet(), 1L, 64L),
      () -> new CharacterContentValidate(form.getFajlnevKezdet(), Constant.FILE_NAME_ENABLED_CHARACTERS)
    );
    
    // Ez a fájlnév kezdet létezik az aktuális könyvtárban ?
    if (form.getFajlnevKezdet() != null) { 
      if (context.enableAddMessage(field)) {
        List<HiddenFile> hiddenFiles = HiddenFile.getFiles(true, null);
        for (HiddenFile hiddenFile: hiddenFiles) {
          if (form.getFajlnevKezdet().equals(hiddenFile.getBaseName())) {
            context.add(field, "Ilyen nevű fájl már létezik!");
          }
        }  
      }
    }  
    
    // jelszoTipus
    field = "jelszoTipus";
    context.add(field, 
      () -> new RequiredValidate(form.getJelszoTipus())
    );

    // jelszo
    field = "jelszo";
    context.add(field, 
      () -> new LengthValidate(form.getJelszo(), 1L, 64L),
      () -> new PasswordSyntaxValidate(form.getJelszo(), Password.Type.valueOf(form.getJelszoTipus()))
    );

    // jelszo2
    field = "jelszo2";
    context.add(field, 
      () -> new LengthValidate(form.getJelszo2(), 1L, 64L),
      () -> new PasswordSyntaxValidate(form.getJelszo2(), Password.Type.valueOf(form.getJelszoTipus()))
    );
    
    // Egyezik a két jelszó ?
    if (!result.hasFieldErrors("jelszo") && !result.hasFieldErrors("jelszo2")) {
      if (!StringTool.equal(form.getJelszo(), form.getJelszo2())) {
        context.add(null, "Nem egyezik a két jelszó!");
      }
    }
    
    // Ki lett legalább egy mező töltve ?
    if (form.getFajlnevKezdet() == null && form.getJelszo() == null && form.getJelszo2() == null) {
      context.add(null, "Legalább egy helyen szükséges adatot megadni!");
    }
    
  }
  
  /**
   * Ellenőrzés
   */
  private void check() {

    // A nyitott fájl aktuális ?
    checkUtil.isOpenedActual();
    
  }
  
  // ====
}
