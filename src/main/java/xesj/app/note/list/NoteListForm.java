package xesj.app.note.list;
import lombok.Getter;
import lombok.Setter;

/**
 * Bejegyzések listázása form
 */
@Getter
@Setter
public class NoteListForm {
  
  private String fragment;
  private String submit;

}
