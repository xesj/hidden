package xesj.app.note.list;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import xesj.app.system.MainSession;
import xesj.app.util.main.CheckUtil;
import xesj.app.util.main.Constant;
import xesj.tool.StringTool;

/**
 * Bejegyzések listázása controller
 */
@Controller
public class NoteListController {
  
  public static final String PATH = "/note/list";
  private static final String VIEW = "/note/note-list.html";

  @Autowired MainSession session;
  @Autowired CheckUtil checkUtil;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Bejegyzések");
    model.addAttribute("editable", session.isOpenedActual());
    
  }
  
  /**
   * GET: /note/list
   */
  @GetMapping(PATH)
  public String get(Model model) {
    
    // Ellenőrzés
    check();
    
    // Form beállítása
    NoteListForm form = Optional.ofNullable(session.getNoteListForm()).orElse(new NoteListForm());
    
    // Bejegyzés nevek listája
    List<String> notes = session.getOpenedFile().getNoteNames(form.getFragment());
    
    // Model beállítása
    model.addAttribute("form", form);
    model.addAttribute("notes", notes);
    
    // View megjelenítése
    return VIEW;

  }

  /**
   * POST: /note/list
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") NoteListForm form, BindingResult result,  Model model) {
    
    // Ellenőrzés
    check();
   
    // Törlés gomb esetén: feltételek törlése
    if (StringTool.equal(form.getSubmit(), "TORLES")) {
      form.setFragment(null);
    }

    // Form mentése a session-be
    session.setNoteListForm(form);

    // Model beállítása: bejegyzés nevek
    model.addAttribute(
      "notes", 
      session.getOpenedFile().getNoteNames(form.getFragment())
    );

    // View megjelenítése
    return VIEW;
    
  }
  
  /**
   * Ellenőrzés
   */
  private void check() {

    // Van nyitott fájl ?
    checkUtil.hasOpenedFile();
    
  }
  
  // ====
}
