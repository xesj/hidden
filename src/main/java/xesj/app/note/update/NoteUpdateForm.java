package xesj.app.note.update;
import lombok.Getter;
import lombok.Setter;

/**
 * Bejegyzés létrehozása/módosítása form
 */
@Getter
@Setter
public class NoteUpdateForm {

  private String noteNameOriginal;  
  private String noteName;  
  private String noteText;  

}
