package xesj.app.note.update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xesj.app.note.view.NoteViewController;
import xesj.app.system.MainSession;
import xesj.app.util.main.CheckUtil;
import xesj.app.util.main.Constant;
import xesj.app.util.main.MainUtil;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.note.NoteUtil;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.RequiredValidate;
import xesj.tool.StringTool;

/**
 * Bejegyzés létrehozása/módosítása controller
 */
@Controller
public class NoteUpdateController {
  
  public static final String PATH = "/note/update";
  private static final String VIEW = "/note/note-update.html";

  @Autowired MainSession session;
  @Autowired CheckUtil checkUtil;
  @Autowired MainUtil mainUtil;
  @Autowired NoteUtil noteUtil;
  
  /**
   * GET: /note/update
   */
  @GetMapping(PATH)
  public String get(@RequestParam(required = false) String note, Model model) {
    
    // Ellenőrzés
    check(note);

    // Model default beállítása
    model.addAttribute(Constant.PAGE_TITLE, 
      (note == null ? "Új bejegyzés" : "Bejegyzés módosítása")
    );
    
    // Form
    NoteUpdateForm form = new NoteUpdateForm();
    if (note != null) {
      form.setNoteNameOriginal(note);
      form.setNoteName(note);
      form.setNoteText(session.getOpenedFile().getNotes().get(note));
    }
    model.addAttribute("form", form);
    
    // VIEW megjelenítése
    return VIEW;

  }

  /**
   * POST: /note/update
   */
  @PostMapping(PATH)
  public String get(@ModelAttribute("form") NoteUpdateForm form, BindingResult result, 
    RedirectAttributes redirectAttributes, Model model) throws Exception {
    
    // Ellenőrzés
    check(form.getNoteNameOriginal());
    
    // Validáció
    validate(form, result);
    
    // Volt validációs hiba ?
    if (result.hasErrors()) {
      return VIEW;
    }

    // Hidden fájl 
    HiddenFile hiddenFile = session.getOpenedFile();
    
    // Fájl információk tárolása, az "old" könyvtárba mozgatáshoz
    hiddenFile.oldInfo();
    
    // Bejegyzés tárolása
    var notes = hiddenFile.getNotes();
    if (form.getNoteNameOriginal() != null) {
      notes.remove(form.getNoteNameOriginal());
    }    
    notes.put(form.getNoteName(), form.getNoteText());
    
    // Fájl mentése
    hiddenFile.save();
    
    // Redirect a bejegyzés megtekintésére
    redirectAttributes.addAttribute("note", form.getNoteName());
    return Constant.REDIRECT + NoteViewController.PATH;

  }

  /**
   * Validáció
   */
  private void validate(NoteUpdateForm form, BindingResult result) {

    // Előkészítés
    var context = mainUtil.createFormValidationContext(result, 1, null);
    String field;
    
    // Bejegyzés név trim
    if (form.getNoteName() != null) {
      form.setNoteName(form.getNoteName().trim());
    }

    // Bejegyzés szöveg trim
    form.setNoteText(noteUtil.trim(form.getNoteText()));  
    
    // NoteName
    field = "noteName";
    context.add(field,
      () -> new RequiredValidate(form.getNoteName()),
      () -> new LengthValidate(form.getNoteName(), 1L, 128L)
    );
    if (!result.hasFieldErrors(field)) {
      if (form.getNoteNameOriginal() == null) {
        // Új bejegyzés
        if (session.hasOpenedNote(form.getNoteName())) {
          context.add(field, "Ez a bejegyzés név már létezik!");
        }
      }
      else {
        // Bejegyzés módosítás
        if (!form.getNoteNameOriginal().equals(form.getNoteName())) {
          if (session.hasOpenedNote(form.getNoteName())) {
            context.add(field, "Ez a bejegyzés név már létezik!");
          }
        }
      }
    }
    
    // NoteText
    field = "noteText";
    context.add(field, () -> new LengthValidate(form.getNoteName(), 0L, 65536L));

  }
  
  /**
   * Ellenőrzés
   * @param note Bejegyzés neve.
   */
  private void check(String note) {

    // Van nyitott fájl ?
    checkUtil.hasOpenedFile();

    // A nyitott fájl aktuális ?
    checkUtil.isOpenedActual();
    
    // Ha meg van adva a bejegyzés név, az létezik a nyitott fájlban ?
    if (note != null) checkUtil.hasOpenedNote(note);
    
  }
  
  // ====
}
