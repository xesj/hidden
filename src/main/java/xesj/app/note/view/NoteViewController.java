package xesj.app.note.view;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.app.system.MainSession;
import xesj.app.util.main.CheckUtil;
import xesj.app.util.main.Constant;
import xesj.app.util.note.NoteUtil;

/**
 * Bejegyzés megtekintése controller
 */
@Controller
public class NoteViewController {
  
  public static final String PATH = "/note/view";
  private static final String VIEW = "/note/note-view.html";

  @Autowired MainSession session;
  @Autowired NoteUtil noteUtil;
  @Autowired CheckUtil checkUtil;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Bejegyzés megtekintése");
    
  }
  
  /**
   * GET: /note/view
   */
  @GetMapping(PATH)
  public String get(@RequestParam String note, Model model) {
    
    // Ellenőrzés
    check(note);
    
    // Bejegyzés szövege
    String noteText = session.getOpenedFile().getNotes().get(note);
   
    // Model beállítása
    model.addAttribute("noteName", note);
    model.addAttribute("noteRows", noteUtil.viewFormat(noteText));
    
    // View megjelenítése
    return VIEW;

  }
  
  /**
   * Ellenőrzés
   * @param note Bejegyzés neve.
   */
  private void check(String note) {

    // Van nyitott fájl ?
    checkUtil.hasOpenedFile();
    
    // Létezik a bejegyzés név a nyitott fájlban ?
    checkUtil.hasOpenedNote(note);
    
  }
  
  // ====
}
