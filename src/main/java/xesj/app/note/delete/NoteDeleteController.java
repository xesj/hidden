package xesj.app.note.delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.app.note.list.NoteListController;
import xesj.app.system.MainSession;
import xesj.app.util.main.CheckUtil;
import xesj.app.util.main.Constant;
import xesj.app.util.file.HiddenFile;

/**
 * Bejegyzés törlése controller
 */
@Controller
public class NoteDeleteController {
  
  public static final String PATH = "/note/delete";

  @Autowired MainSession session;
  @Autowired CheckUtil checkUtil;
  
  /**
   * GET: /note/delete
   */
  @GetMapping(PATH)
  public String get(@RequestParam String note, Model model) throws Exception {
    
    // Ellenőrzés
    check(note);
    
    // Hidden fájl 
    HiddenFile hiddenFile = session.getOpenedFile();
    
    // Fájl információk tárolása, az "old" könyvtárba mozgatáshoz
    hiddenFile.oldInfo();
    
    // Bejegyzés törlése
    hiddenFile.getNotes().remove(note);
    
    // Fájl mentése
    hiddenFile.save();
    
    // Redirect a bejegyzések listájára
    return Constant.REDIRECT + NoteListController.PATH;

  }
  
  /**
   * Ellenőrzés
   * @param note Bejegyzés neve.
   */
  private void check(String note) {

    // Van nyitott fájl ?
    checkUtil.hasOpenedFile();

    // A nyitott fájl aktuális ?
    checkUtil.isOpenedActual();
    
    // Létezik a bejegyzés név a nyitott fájlban ?
    checkUtil.hasOpenedNote(note);
    
  }
  
  // ====
}
