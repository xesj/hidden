package xesj.app.system;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import xesj.app.util.main.Constant;
import xesj.app.util.main.ErrorData;
import xesj.app.util.exception.ConcurrentException;
import xesj.app.util.exception.FileSizeException;
import xesj.app.util.exception.UnitException;

/**
 * 3-as szintű exception handler
 */
@Controller
public class MainErrorController implements ErrorController {

  public static final String PATH = "/error";
  private static final String 
    VIEW_UNIT_EXCEPTION = "/exception/unit-exception.html",
    VIEW_FILE_SIZE_EXCEPTION = "/exception/file-size-exception.html",
    VIEW_CONCURRENT_EXCEPTION = "/exception/concurrent-exception.html",
    VIEW_UNEXPECTED_EXCEPTION = "/exception/unexpected-exception.html";

  @Autowired ErrorAttributes errorAttributes;
  @Autowired MainSession session;
  
  /**
   * Error handler
   */
  @RequestMapping(PATH)
  public String errorHandler(WebRequest webRequest, Model model) {

    // Error objektum előállítása
    ErrorData errorData = new ErrorData(errorAttributes, webRequest);
    
    // Model beállítása
    model.addAttribute("errorData", errorData);
    
    // Error handler hívása
    try {
      throw errorData.throwable;
    }
    catch (UnitException unitException) {
      return unitExceptionHandler(unitException, model);
    }
    catch (FileSizeException fileSizeException) {
      return fileSizeExceptionHandler(fileSizeException, model);
    }
    catch (ConcurrentException concurrentException) {
      return concurrentExceptionHandler(concurrentException, model);
    }
    catch (Throwable t) {
      model.addAttribute(Constant.PAGE_TITLE, "Váratlan hiba");
      return VIEW_UNEXPECTED_EXCEPTION;
    }
    
  }
  
  /**
   * UnitException handler
   */
  private String unitExceptionHandler(UnitException unitException, Model model) {

    model.addAttribute(Constant.PAGE_TITLE, "Hiba");
    model.addAttribute("actualUnit", unitException.actualUnit);
    model.addAttribute("oldUnit", unitException.oldUnit);
    model.addAttribute("allUnit", unitException.actualUnit + unitException.oldUnit);
    model.addAttribute("maximumUnit", 
      (session.getTicket() == null ? "?" : session.getTicket().getUnit())
    );
    return VIEW_UNIT_EXCEPTION;
    
  }

  /**
   * FileSizeException handler
   */
  private String fileSizeExceptionHandler(FileSizeException fileSizeException, Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Hiba");
    model.addAttribute("fileSize", fileSizeException.fileSize);
    model.addAttribute("maxInText", 
      (session.getTicket() == null ? "?" : session.getTicket().getSize())
    );
    model.addAttribute("maxInByte", 
      (session.getTicket() == null ? "?" : session.getTicket().getSizeBytes())
    );
    return VIEW_FILE_SIZE_EXCEPTION;
    
  }

  /**
   * ConcurrentException handler
   */
  private String concurrentExceptionHandler(ConcurrentException concurrentException, Model model) {

    model.addAttribute(Constant.PAGE_TITLE, "Hiba");
    session.setOpenedFile(null);
    return VIEW_CONCURRENT_EXCEPTION;
    
  }
  
  // ====
}
