package xesj.app.system;
import java.util.LinkedHashMap;
import java.util.Map;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xesj.app.main.LoginTicketController;
import xesj.app.file.list.FileListController;
import xesj.app.util.main.Constant;

/**
 * Fő controller
 */
@Controller
public class MainController {

  public static final String 
    PATH_ROOT = "/",
    PATH_PING = "/ping",
    PATH_LOGOUT = "/logout";
  
  private static final String
    VIEW_LOGOUT = "/main/logout.html";
  
  @Autowired MainApplication application;
  @Autowired MainSession session;
  @Autowired HttpSession httpSession;
  
  /**
   * GET: /
   * Ha a kapott "ticket" paraméter ki van töltve, és ezzel van a felhasználó bejelentkezve, 
   * akkor a fájlok listájára redirect-ál. 
   * Ellenkező esetben a ticket login-ra redirect-ál.
   * @param ticket Meghívó
   */
  @GetMapping(PATH_ROOT)
  public String get(@RequestParam(required = false) String ticket, RedirectAttributes redirectAttributes) {
 
    // Redirect a fájlok listájára, ha lehetséges
    if (ticket != null && session.isTicketLogged() && session.getTicket().getId().equals(ticket)) {
      return Constant.REDIRECT + FileListController.PATH;
    } 
      
    // Redirect a ticket loginra
    redirectAttributes.addAttribute("ticket", ticket);
    return Constant.REDIRECT + LoginTicketController.PATH;

  }
  
  /**
   * GET: /ping
   * @return HSDE válasz
   */
  @GetMapping(PATH_PING)
  @ResponseBody
  public Map ping() {
    
    // Válasz összeállítása
    Map<String, Object> map = new LinkedHashMap<>();
    map.put("data", null);
    map.put("error", null);
    return map;

  }

  /**
   * GET: /logout
   */
  @GetMapping(PATH_LOGOUT)
  public String logout(Model model) {
    
    // Session érvénytelenítés
    httpSession.invalidate();
    
    // Session timeout meghatározása percben
    model.addAttribute("timeout", application.getSessionTimeout(0) / 60);

    // Oldalcím    
    model.addAttribute(Constant.PAGE_TITLE, "Kilépés");

    // View megjelenítése
    return VIEW_LOGOUT;

  }
  
  // ====
}
