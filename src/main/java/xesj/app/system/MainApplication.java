package xesj.app.system;
import jakarta.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Alkalmazás szintű fő bean
 */
@Service
@Getter
@Setter
public class MainApplication {

  @Autowired HttpSession httpSession;
  @Autowired Property property;

  /**
   * Application context nem spring bean-ek számára
   */
  public static ApplicationContext context;
  
  /**
   * Titkos salt értéke bájt tömbként
   */
  private byte[] saltHidden;
  
  /**
   * Logout path lekérdezése
   */
  public String getLogoutPath() {

    return property.getContextPath() + MainController.PATH_LOGOUT;

  }  

  /**
   * Ping path lekérdezése
   */
  public String getPingPath() {

    return property.getContextPath() + MainController.PATH_PING;

  }  
  
  /**
   * Session timeout másodpercben megadva, a differencia hozzáadásával.
   * @param diff Differencia másodpercben. Negatív érték esetén csökken az érték.
   */
  public int getSessionTimeout(int diff) {
    
    return httpSession.getMaxInactiveInterval() + diff;
    
  }

  // ====
}
