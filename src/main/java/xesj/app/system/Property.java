package xesj.app.system;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import xesj.app.util.password.Password;

/**
 * Alkalmazás jellemzők
 */
@Service
@ConfigurationProperties("app")
@Validated
@Getter
@Setter
public class Property {

  @Value("${server.servlet.context-path}") 
  private String contextPath;
  
  @NotNull
  private String programName;
  
  @NotNull 
  private String baseDir;
  
  @NotNull
  private String salt;

  private String autoLoginTicket;
  
  private String autoOpenFile;
  
  private Boolean autoOpenActual;
  
  private String autoOpenPassword;
  
  private Password.Type autoOpenPasswordType;
  
  // ====
}
