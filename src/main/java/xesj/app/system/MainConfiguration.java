package xesj.app.system;
import java.time.LocalDate;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import xesj.app.main.LoginPasswordController;
import xesj.app.main.LoginTicketController;
import xesj.app.test.TestController;
import xesj.spring.property_editor.DatePropertyEditor;
import xesj.spring.property_editor.LocalDatePropertyEditor;
import xesj.spring.property_editor.StringPropertyEditor;
import xesj.tool.LocaleTool;

/**
 * Fő konfiguráció 
 */
@Configuration
@ControllerAdvice
public class MainConfiguration implements WebMvcConfigurer {
  
  @Autowired LoginInterceptor loginInterceptor;
  
  /**
   * Interceptor regisztrációk
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {

    // WebContent interceptor regisztráció. Cache letiltva a weblapok számára.
    WebContentInterceptor wci = new WebContentInterceptor();
    wci.addCacheMapping(CacheControl.noStore(), "/**");
    registry.addInterceptor(wci).excludePathPatterns("/static/**").order(1);

    // Login interceptor regisztráció
    registry.addInterceptor(loginInterceptor).excludePathPatterns(
      "/error",  
      "/static/**",
      MainController.PATH_ROOT,
      MainController.PATH_PING,
      MainController.PATH_LOGOUT,
      LoginTicketController.PATH,
      LoginPasswordController.PATH,
      TestController.PATH
    ).order(2);
    
  }
  
  /**
   * Globális initbinder
   */
  @InitBinder
  public void initBinder(WebDataBinder binder) throws Exception {
    
    binder.registerCustomEditor(String.class, new StringPropertyEditor());
    binder.registerCustomEditor(Date.class, new DatePropertyEditor("yyyy.MM.dd"));
    binder.registerCustomEditor(LocalDate.class, new LocalDatePropertyEditor("uuuu.MM.dd"));

  }
  
  /**
   * Locale beállítása fixen magyarra
   */
  @Bean
  public LocaleResolver localeResolver() {
    
    return new FixedLocaleResolver(LocaleTool.LOCALE_HU);

  }
 
  /**
   * Cache engedélyezve a static resource-ok számára. A public cache-ben 17 percig tárolhatók.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {

    registry
      .addResourceHandler("/**")
      .addResourceLocations("classpath:/public/")
      .setCacheControl(CacheControl.maxAge(1024, TimeUnit.SECONDS).mustRevalidate().cachePublic());

  }

  // ====
}
