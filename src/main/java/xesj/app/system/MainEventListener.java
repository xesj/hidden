package xesj.app.system;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;
import lombok.NonNull;
import xesj.app.util.exception.BadSaltException;
import xesj.app.util.exception.HiddenException;
import xesj.tool.ByteTool;

/**
 * Fő event listener
 */
@Service
public class MainEventListener {

  public static final Logger LOGGER = Logger.getLogger(MainEventListener.class.getName());
  
  @Autowired ApplicationContext applicationContext;
  @Autowired Property property;
  @Autowired MainApplication application;
 
  /**
   * Webserver indítása előtt futó eljárás
   */
  @EventListener
  public void onApplicationEvent(ContextRefreshedEvent event) throws IOException {
    
    // Application context beállítása statikus változóba nem spring-bean-ek számára
    MainApplication.context = applicationContext;

    // Beállítások logolása
    File baseDirFile = new File(property.getBaseDir()); 
    LOGGER.info("Working directory: " + System.getProperty("user.dir"));
    LOGGER.info("Base directory: " + baseDirFile.getCanonicalPath());
    LOGGER.info("Temporary directory: " + System.getProperty("java.io.tmpdir"));
    LOGGER.info("Maximum memory usage: " + Runtime.getRuntime().maxMemory());
    
    // Ellenőrzés: létezik a base directory ?
    if (!baseDirFile.exists()) {
      throw new HiddenException("Nem létezik a base directory!");
    }

    // Ellenőrzés: olvasható a base directory ?
    if (!baseDirFile.canRead()) {
      throw new HiddenException("Nem olvasható a base directory!");
    }

    // Ellenőrzés: írható a base directory ?
    if (!baseDirFile.canWrite()) {
      throw new HiddenException("Nem írható a base directory!");
    }
    
    // Salt paraméter bájt tömbre alakítása
    application.setSaltHidden(
      saltToBytes(property.getSalt())
    );

  }
  
  /**
   * Salt paraméter bájt tömbre alakítása.
   * @param salt Salt hexadecimális formában, ahogy az alkalmazás indításkor megkapja.
   * @return Salt bájt-sorozatként.
   * @throws BadSaltException Ha a salt nem megfelelő.
   */
  public byte[] saltToBytes(@NonNull String salt) throws BadSaltException {

    // Hossz ellenőrzése
    if (salt.length() < 32 || salt.length() > 512) {
      throw new BadSaltException("A 'salt' csak 32 - 512 karakteres lehet!");
    }
    
    // Konverzió
    byte[] bytes;
    try {
      bytes = ByteTool.hexToByteArray(salt);
    }  
    catch (NumberFormatException nfe) {
      throw new BadSaltException("Hibás 'salt'! " + nfe.getMessage());
    }
    
    // Válasz
    return bytes;
    
  }
  
  // ====
}
