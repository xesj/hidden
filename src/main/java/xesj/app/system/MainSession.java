package xesj.app.system;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import xesj.app.file.list.FileListForm;
import xesj.app.note.list.NoteListForm;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.main.Constant;
import xesj.app.util.password.Password;
import xesj.app.util.ticket.Ticket;

/**
 * Session szintű fő bean 
 */
@Service
@SessionScope
@Getter
@Setter
public class MainSession {
  
  /**
   * Meghívó. Belépés után kitöltöttnek kell lennie.
   */
  private Ticket ticket;
  
  /**
   * Jelszavak. Ha a ticket alatt egyetlen hidden fájl sem létezik, akkor a belépéskor nem szükséges jelszót megadni,
   * és ezt az állapotot egy üres listával jelezzük. A jelszavak száma maximálva van.
   */
  private List<Password> passwords;
  
  /**
   * Megnyitott (dekódolt) hidden fájl. Ha még nincs ilyen, akkor null.
   */
  private HiddenFile openedFile;
  
  /**
   * Fájl lista form-ja. Megőrizzük, hogy megmaradjanak a szűrők ha a felhasználó visszatér a lapra.
   */
  FileListForm fileListForm;

  /**
   * Bejegyzés lista form-ja. Megőrizzük, hogy megmaradjanak a szűrők ha a felhasználó visszatér a lapra.
   */
  NoteListForm noteListForm;
  
  /**
   * Megtörtént-e a bejelentkezés a meghívóval ?
   */
  public boolean isTicketLogged() {
    
    return (ticket != null);
    
  }

  /**
   * Megtörtént-e a bejelentkezés a jelszóval ?
   */
  public boolean isPasswordLogged() {
    
    return (passwords != null);
    
  }

  /**
   * Megtörtént-e a teljes bejelentkezés a meghívóval és a jelszóval ?
   */
  public boolean isLogged() {
    
    return (isTicketLogged() && isPasswordLogged());
    
  }
  
  /**
   * Van nyitott fájl ?
   */
  public boolean hasOpenedFile() {
    
    return (openedFile != null);
    
  }
  
  /**
   * A megnyitott fájl aktuális ?
   * @return True: ha van megnyitott fájl, és aktuális. Különben false.
   */
  public boolean isOpenedActual() {
    
    return (hasOpenedFile() && openedFile.isActual());
    
  }  
    
  /**
   * A megnyitott fájlban létezik ez a bejegyzés név ?
   * @return True: ha van megnyitott fájl, és tartalmazza a bejegyzés nevet. Különben false.
   */
  public boolean hasOpenedNote(@NonNull String noteName) {

    return (hasOpenedFile() && openedFile.hasNote(noteName));
    
  }
  
  /**
   * Berakja a jelszót a password-listába, de ügyel arra, hogy a tárolt jelszavak száma
   * ne haladja meg a megengedett maximumot.
   * @param password Jelszó
   * @param loginPassword 
   *        True: a password-lista csak ezt a jelszót fogja tartalmazni, hiszen login-nál írták be.
   *        False: hozzáadja a jelszót a password-lista többi eleméhez. 
   */  
  public void add(Password password, boolean loginPassword) {
  
    // Password lista kezelése
    if (loginPassword) {
      // Hozzáadás egy üres password listához
      passwords = new ArrayList<>();
      passwords.add(password);
    }
    else {
      // Tárolás a password lista első helyére. Ha túl sok elemű a lista, akkor az utolsó elemeket törli.
      while (passwords.size() >= Constant.MAXIMUM_PASSWORD_PER_SESSION) {
        passwords.remove(passwords.size() - 1); // Utolsó elem törlése
      }
      passwords.add(0, password); // Beszúrás első elemnek
    }
    
  } 

  // ====
}
