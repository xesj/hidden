package xesj.app.system;
import java.util.ArrayList;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import xesj.app.main.LoginPasswordController;
import xesj.app.main.LoginTicketController;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.password.Password;
import xesj.app.util.ticket.Ticket;

/**
 * Az osztály ellenőrzi hogy a felhasználó be van-e jelentkezve. 
 * Ha nincs, akkor a megfelelő bejelentkező lapra redirect-ál.
 */
@Service
public class LoginInterceptor implements HandlerInterceptor {
  
  @Autowired MainSession session;
  @Autowired Property property;

  /**
   * Műveletek a controllerek lefutása előtt
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {

    // Automatikus login, open (feltételes)
    autoLoginOpen();
    
    // Megtörtént a ticket login ?
    if (!session.isTicketLogged()) {
      response.sendRedirect(request.getContextPath() + LoginTicketController.PATH);
      return false;
    }
      
    // Megtörtént a password login ?
    if (!session.isPasswordLogged()) {
      response.sendRedirect(request.getContextPath() + LoginPasswordController.PATH);
      return false;
    }

    // A felhasználó be van jelentkezve, a kérés engedélyezve.
    return true;

  }

  /**
   * Automatikus login, open.
   * Automatikus login akkor történik ha be van állítva a ticket, és még nem történt meg a egyik login sem.
   * Automatikus open (hidden fájl dekódolás) akkor történik, ha be van állítva, és a MainSession még nem
   * tartalmaz dekódolt fájlt. Automatikus open csak akkor történhet ha van automatikus login is.
   */
  private void autoLoginOpen() throws Exception {
    
    // Automatikus login ticket-je be van állítva ?
    if (property.getAutoLoginTicket() == null) {
      return; // Nincs beállítva
    } 
    
    // Automatikus login, ha még nincs bejelentkezve
    if (!session.isTicketLogged() && !session.isPasswordLogged()) {
      Ticket ticket = new Ticket(property.getAutoLoginTicket());
      session.setTicket(ticket);
      session.setPasswords(new ArrayList<>());
    }  
    
    // Automatikus open minden adata be van állítva ?
    if (property.getAutoOpenFile() == null || property.getAutoOpenActual() == null || 
      property.getAutoOpenPassword() == null || property.getAutoOpenPasswordType() == null) {
      return; // Nincs beállítva
    } 

    // Automatikus open, ha még nincs megnyitva hidden fájl
    if (!session.hasOpenedFile()) {
      HiddenFile hiddenFile = new HiddenFile(property.getAutoOpenFile(), property.getAutoOpenActual());
      Password password = new Password(property.getAutoOpenPassword(), property.getAutoOpenPasswordType());
      hiddenFile.open(password);
      session.add(password, true);
      session.setOpenedFile(hiddenFile);
    }

  }

  // ====
}
