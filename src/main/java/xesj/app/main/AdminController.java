package xesj.app.main;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import xesj.app.util.main.Constant;

/**
 * Adminisztráció controller
 */
@Controller
public class AdminController {
  
  public static final String PATH = "/admin";
  private static final String VIEW = "/main/admin.html";
  
  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Adminisztráció");
    
  }
  
  /**
   * GET: /admin
   */
  @GetMapping(PATH)
  public String get() {
    
    return VIEW;
    
  }
  
  // ====
}
