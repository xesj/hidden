package xesj.app.main;
import lombok.Getter;
import lombok.Setter;

/**
 * Password belépés form
 */
@Getter
@Setter
public class LoginPasswordForm {

  /**
   * Jelszó típusa. "TEXT" = szöveges, "HEXA" = hexadecimális 
   */
  private String tipus; 
  
  /**
   * Jelszó
   */
  private String jelszo;  

}
