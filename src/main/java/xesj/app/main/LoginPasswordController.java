package xesj.app.main;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xesj.app.file.list.FileListController;
import xesj.app.file.warning.FileWarningController;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.main.MainUtil;
import xesj.app.util.file.HiddenFile;
import xesj.app.util.password.Password;
import xesj.app.util.password.PasswordValidate;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.RequiredValidate;
import xesj.spring.validation.validate.SetValidate;

/**
 * Password belépés controller
 */
@Controller
public class LoginPasswordController {
  
  public static final String PATH = "/login/password";
  private static final String VIEW = "/main/login-password.html";
  
  @Autowired MainSession session;
  @Autowired MainUtil mainUtil;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Belépés, jelszó megadása");
    model.addAttribute("typeOptions", Password.getTypeOptions());
    
  }
  
  /**
   * GET: /login/password
   */
  @GetMapping(PATH)
  public String getLoginTicket(Model model) {
    
    // Ellenőrzés: létezik ticket ?
    if (!session.isTicketLogged()) {
      return Constant.REDIRECT + LoginTicketController.PATH;
    }
    
    // Létezik aktuális vagy régi hidden fájl ?
    if (HiddenFile.getFiles(true, null).isEmpty() && HiddenFile.getFiles(false, null).isEmpty()) {
      List<Password> emptyList= new ArrayList<>();
      session.setPasswords(emptyList);
      return Constant.REDIRECT + FileListController.PATH;
    }
    
    // Model beállítása
    LoginPasswordForm form = new LoginPasswordForm();
    form.setTipus(Password.Type.TEXT.name());
    model.addAttribute("form", form);
    
    // View megjelenítése
    return VIEW;
    
  }

  /**
   * POST: /login/password
   */
  @PostMapping(PATH)
  public String postLoginTicket(@ModelAttribute("form") LoginPasswordForm form, BindingResult result, Model model, 
    RedirectAttributes redirectAttributes) throws UnsupportedEncodingException {

    // Ellenőrzés: létezik ticket ?
    if (!session.isTicketLogged()) {
      return Constant.REDIRECT + LoginTicketController.PATH;
    }
    
    // Validáció
    validate(form, result);
    
    // Validációs hiba esetén saját view megjelenítése
    if (result.hasErrors()) {
      return VIEW;
    }

    // A helyes jelszó bejegyzése a password-listába
    Password password = new Password(form.getJelszo(), Password.Type.valueOf(form.getTipus()));
    session.add(password, true);
    
    // Redirect: figyelmeztetések 
    redirectAttributes.addAttribute("visible", false); 
    return Constant.REDIRECT + FileWarningController.PATH;
    
  }
  
  /**
   * Validáció
   */
  private void validate(LoginPasswordForm form, BindingResult result) {
    
    // Előkészítés
    FormValidationContext context = mainUtil.createFormValidationContext(result, 1, null);
    String field;
    
    // Tipus
    field = "tipus";
    context.add(field, 
      () -> new RequiredValidate(form.getTipus()),
      () -> new SetValidate(form.getTipus(), Password.Type.TEXT.name(), Password.Type.HEXA.name())
    );

    // Jelszo
    field = "jelszo";
    context.add(field, 
      () -> new RequiredValidate(form.getJelszo(), false),
      () -> new LengthValidate(form.getJelszo(), 1L, 64L)
    );
    
    // Megnyitható a jelszóval legalább egy fájl ? 
    if (!result.hasErrors()) {
      context.add(field, 
        () -> new PasswordValidate(form.getJelszo(), Password.Type.valueOf(form.getTipus()), null, true)
      );
    }
    
  }
  
  // ====
}
