package xesj.app.main;
import java.util.logging.Logger;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.app.system.MainSession;
import xesj.app.util.main.Constant;
import xesj.app.util.main.MainUtil;
import xesj.app.util.ticket.Ticket;
import xesj.app.util.ticket.TicketValidate;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.ValidationContext;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.RequiredValidate;

/**
 * Ticket belépés controller
 */
@Controller
public class LoginTicketController {
  
  public static final String PATH = "/login/ticket";
  private static final String VIEW = "/main/login-ticket.html";
  public static final Logger LOGGER = Logger.getLogger(LoginTicketController.class.getName());
  
  @Autowired MainSession session;
  @Autowired MainUtil mainUtil;
  @Autowired HttpSession httpSession;

  /**
   * Model default beállítása
   */
  @ModelAttribute
  public void configure(Model model) {
    
    model.addAttribute(Constant.PAGE_TITLE, "Belépés, meghívó megadása");
    
  }
  
  /**
   * GET: /login/ticket
   * @param ticket 
   *        Meghívó. Ha meg van adva, és helyes az értéke, akkor nem megyünk az űrlapra, mert felesleges oldal villanna be.
   */
  @GetMapping(PATH)
  public String getLoginTicket(@RequestParam(required = false) String ticket, Model model) {
    
    // Session érvénytelenítés
    httpSession.invalidate();

    // Form
    LoginTicketForm form = new LoginTicketForm();
    
    // Kaptunk ticket paramétert az url-ben ?
    if (ticket != null) {
      ValidationContext context = mainUtil.createValidationContext(1, null, null);
      validate(ticket, context);
      if (!context.hasMessage()) {
        // A ticket helyes
        log(true, "GET", ticket);
        return goodTicket(ticket); // Tovább a password login oldalra
      }
      else {
        // A ticket hibás, ezért az űrlapot kell megjeleníteni + beállítani egy direkt submit gombnyomást
        form.setTicketShadow(ticket);
        model.addAttribute("submit", true);
        log(false, "GET", ticket);
      }
    }
    
    // Model beállítása
    model.addAttribute("form", form);
    
    // View megjelenítése
    return VIEW;
    
  }

  /**
   * POST: /login/ticket
   */
  @PostMapping(PATH)
  public String postLoginTicket(@ModelAttribute("form") LoginTicketForm form, BindingResult result, Model model) {
    
    // Validáció
    validate(form, result);
    
    // Volt validációs hiba ?
    if (result.hasErrors()) {
      log(false, "POST", form.getTicket());
      return VIEW;
    }
    
    // Helyes ticket lekezelése
    log(true, "POST", form.getTicket());
    return goodTicket(form.getTicket());
    
  }
  
  /**
   * Validáció post után
   */
  private void validate(LoginTicketForm form, BindingResult result) {
    
    // Előkészítés
    FormValidationContext context = mainUtil.createFormValidationContext(result, 1, null);
    String field;
    
    // Ticket
    field = "ticket";
    context.add(field, 
      () -> new RequiredValidate(form.getTicket()),
      () -> new LengthValidate(form.getTicket(), 39L, 39L),
      () -> new TicketValidate(form.getTicket())
    );
    
  }

  /**
   * Validáció get után
   */
  private void validate(String ticket, ValidationContext context) {

    // Ticket
    String field = "ticket";
    context.add(field, 
      () -> new RequiredValidate(ticket),
      () -> new LengthValidate(ticket, 39L, 39L),
      () -> new TicketValidate(ticket)
    );

  }
  
  /**
   * Eljárás helyes ticket esetén
   */
  private String goodTicket(String id) {

    // Ticket létrehozása
    Ticket ticket = new Ticket(id);

    // Ticket bejegyzése a session-be
    session.setTicket(ticket);

    // Redirect a password belépéshez
    return Constant.REDIRECT + LoginPasswordController.PATH;    
    
  }
  
  /**
   * A bejelentkezés log-olása. Az üzenet formátuma sikeres, és sikertelen bejelentkezés esetén:
   * TICKET LOGIN SUCCESS, ip: 111.111.111.111, method: POST, ticket: nnnn-...
   * TICKET LOGIN FAILED, ip: 111.111.111.111, method: GET, ticket: nnnn-nnnn-nnnn-nnnn-nnnn-nnnn-nnnn-nnnn
   */
  private void log(boolean success, String method, String ticket) {
    
    // Üzenet összeállítása
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder
      .append("TICKET LOGIN ")
      .append(success ? "SUCCESS" : "FAILED")
      .append(", ip: ")
      .append(mainUtil.getIP())
      .append(", method: ")
      .append(method)
      .append(", ticket: ")
      .append(success ? ticket.substring(0, 5) : ticket)
      .append(success ? "..." : "");

    // Log írása      
    LOGGER.info(stringBuilder.toString());
    
  }
  
  // ====
}
