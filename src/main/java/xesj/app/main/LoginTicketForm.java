package xesj.app.main;
import lombok.Getter;
import lombok.Setter;

/**
 * Ticket belépés form
 */
@Getter
@Setter
public class LoginTicketForm {

  private String ticket;  
  private String ticketShadow;  

}
