+----------------------------+
| PASSWORD, SALT, SECRET KEY |
--------------------------------------------------------------------------------------------------------------------------------
- A password megadható szövegesen, vagy hexadecimálisan a bájtok értékeit leírva.

- A hidden fájl titkosításához/visszafejtéséhez a program AES-256-GCM algoritmust használ.

- A titkosításhoz/visszafejtéshez tartozó secret-key a felhasználó által begépelt jelszóból és a program indítási
  paraméterként megadott "app.salt"-ból (saltHidden), vagy egy fix salt0-ból áll elő.

- A felhasználó által begépelt jelszóhoz 2 secret-key áll elő, melyek a Password.java osztályban láthatók:

  - secretKey       <--  A password + saltHidden kombinálásával áll elő  
  - secretKeySalt0  <--  A password + salt0 kombinálásával áll elő 

- Ha a hidden-fájl felhasználó által megadott (baseName) része "-salt0" végződésű akkor a titkosítás/visszafejtés
  a secretKeySalt0 kulccsal, különben pedig a secretKey kulccsal történik.

- Az AES-256-GCM algoritmus a GCM miatt egy initialization vectort (IV) is használ, 
  mely a biztonság érdekében minden titkosításnál más. Ez a fix 16 bájtos érték bekerül a hidden fájlba is,
  hogy a visszafejtő rátaláljon. Az IV hozzáfűzése a titkosított fájlhoz egy elterjedt eljárás a kriptográfiában.

--------------------------------------------------------------------------------------------------------------------------------
SALT CONVERSION

- Mivel a hidden rendszerek különböző salt értéket használhatnak, ezért egy hidden fájlt nem lehet csak úgy áttenni egyik 
  rendszerből a másikba, mert a különböző saltHidden miatt nem lehetne megnyitni (más-más secret-key állna elő).
  Ilyen esetben a fájl nevét módosítani kell a program "fájl átnevezése" funkciójával úgy, hogy "-salt0" végződést kapjon.
  A program ekkor a fájl titkosításához a "salt0"-t fogja használni. Ez a fájl már átvihető egy másik hidden
  rendszerbe, mely a fájlnév miatt szintén "salt0"-val fogja megnyitni. 
  Ezután az új rendszerben szintén átnevezhető a fájl, a nevéből törölhető a "-salt0". 
  Így a fájlt a másik hidden rendszer a saját saltHidden értékével fogja titkosítani.
 
--------------------------------------------------------------------------------------------------------------------------------