+------------------+
| NOTE (BEJEGYZÉS) |
--------------------------------------------------------------------------------------------------------------------------------
BEJEGYZÉS NÉV

- A bejegyzés név megadása kötelező, hossza 1-128 karakter lehet. 
  A program trim()-mel levágja a felhasználó által rögzített adatot, így nem fog whitespace karakterrel kezdődni/végződni. 
  Egy fájlon belül egyedinek kell lennie a bejegyzés névnek.

--------------------------------------------------------------------------------------------------------------------------------
BEJEGYZÉS SZÖVEG

- Nem kötelező kitölteni. A maximális hossza: 65536 karakter. 

- A bejegyzés szöveg megtekintéskor hűen visszadja a bejegyzést ahogy rögzítve lett (space-ek száma, üres sorok),
  kivéve hogy a bejegyzés felvitelekor a szöveg elejéről és végéről az üres sorok törlődnek. 

- A bejegyzés szöveg tetszőleges html entitást tartalmazhat, az hűen (karakterenként) megjelenik.

- Speciális jelölések is használhatók, ezáltal a tartalma vizuálisan kiemelhető, illetve kattintható link is készíthető:

    Ha egy sort ki szeretnél emelni a szövegből "magasabb" szinten, akkor a sort ezzel kell kezdeni:
    =====>

    Ha egy sort ki szeretnél emelni a szövegből "alacsonyabb" szinten, akkor a sort ezzel kell kezdeni:
    ----->

    Ha kattintható linket szeretnél elhelyezni a szövegben, akkor dupla kapcsos zárójelek közé kell tenni:
    {{...}} 

--------------------------------------------------------------------------------------------------------------------------------