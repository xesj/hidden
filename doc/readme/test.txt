+------+
| TEST |
--------------------------------------------------------------------------------------------------------------------------------
TESZT OLDAL

- A program fejlesztését segíti a teszt url-eket felsoroló egyszerű oldal, melyben a program path-ok vannak felsorolva,
  illetve direkt exception-ök válthatók ki. Ez az oldal a program felületéről nem hívható, csak a böngésző címsorából,
  az url-jének begépelésével:

     .../{contextPath}/test  

--------------------------------------------------------------------------------------------------------------------------------
TESZT ESETEK

- baseDir 
  - Nincs megadva: a program nem indul el, a log-ban jól látható a hiba.
  - Nemlétező könyvtár: a program nem indul el, a log-ban jól látható a hiba.

- salt
  - Nincs megadva: a program nem indul el, a log-ban jól látható a hiba.
  - Nem megfelelő a hossza, nem páros karakterből áll, érvénytelen karaktert tartalmaz: 
    A program nem indul el, a log-ban jól látható a hiba.

- Ticket login
  - Szintaktikailag hibás ticket: mezőbe gépelve, vagy url-ben.
  - Érvénytelen ticket: mezőbe gépelve, vagy url-ben.
  - Ticket technikai hibák: nincs ticket.json, nincs "old" könyvtár, ticket.json tartalmának hibái.
  - Ticket login log-fájl megtekintése: a helyes login-nak csak az első 4 karaktere látszik, a hibásnak a teljes ticket.

- Password login
  - Ha nincs fájl akkor fel sem jön a jelszó bekérő.
  - Ha van aktuális fájl akkor egy aktuális jelszavával lehet belépni, régivel nem.
  - Ha nincs aktuális fájl, akkor be lehessen lépni régi fájl jelszavával. 

- Login szeparáció
  - Ha egy böngészőben be van lépve, akkor egy másik böngészőben nincs, egymásra nincsenek hatással.

- Session timeout
  - Egy oldalon 5 percnél tovább időzve, sem lépteti ki a felhasználót, ha rákattint a "tovább" gombra.
  - Ha session timeout következik be, akkor sem lehet a bőngésző vissza gombjával elérni az előző lapok információit.
  - A session timeout a backend oldalon is működik javascript visszaszámláló nélkül. Ha megtörtént a ticket login,
    és a password bekéréskor 5 percen belül nem írjuk be a jelszót, akkor nem lehet a fájlok listája oldalra jutni.

- Kvóta
  - Ha a fájlok száma eléri/túllépi a limit X%-át, akkor a password után figyelmeztető oldal jelenik meg.
  - Ha a egy fájl mérete eléri/túllépi a limit X%-át, akkor a password után figyelmeztető oldal jelenik meg.

- Adminisztráció
  - Töltsük le a fájlokat a saját gépre, utána töröljük a ticket alatt lévő összes fájlt, majd a zip tartalmát tegyük
    vissza a ticket alá. Minden fájlnak és tartalmának rendben kell lennie.

- Bejegyzés szövege
  - A bejegyzés szöveg megtekintéskor hűen visszadja ahogy a bejegyzést felvittük (space-ek száma, üres sorok),
    kivéve hogy a bejegyzés felvitelekor a szöveg elejéről és végéről az üres sorok törölve lettek. 
  - A bejegyzés szöveg tetszőleges html entitást tartalmazhat, az hűen (karakterenként) megjelenik.

- Régi fájl
  - Nem lehet a jellemzőjét módosítani (fájlnév, jelszó)
  - Nem lehet új bejegyzést felvinni
  - Nem lehet bejegyzést módosítani
  - Nem lehet bejegyzést törölni

- Általános
  - A program tegezi a felhasználót, kivéve a külső library üzenetei.
  - Bejegyzés megtekintése, módosítása esetén olyan fontot kell használni, 
    melyben egyértelműen látszanak a karakterek, tipikusan: ilIL.
  - Maximális hosszúságú fájlnév, bejegyzés név, bejegyzés szöveg kinézete megfelelő.
  - A speciális karakterek a bejegyzés szövegében helyesen jelennek meg: link, 2 szintű kiemelés.
  - Támogatott böngészök, melyekkel tesztelni kell: Firefox, Chrome, Edge
  - Fájl nevek sorrendje: baseName ABC, azon belül dátum csökkenő
  - Bejegyzés nevek sorrendje: ABC
  - Hidden fájlhoz való konkurrens hozzáférés felismerése, lekezelése

--------------------------------------------------------------------------------------------------------------------------------