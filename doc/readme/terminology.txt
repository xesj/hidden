+-------------+
| TERMINOLOGY |
--------------------------------------------------------------------------------------------------------------------------------
ALAPOK

- ticket (meghívó)                <-  Ezzel kell bejelentkezni a program használatához
  - valid (érvényes)              <-  Be lehet vele lépni, mert szintaktikailag helyes, és létezik a fájlrendszerben a könyvtár.
  - invalid (érvénytelen)         <-  Nem lehet vele belépni, mert nincs ilyen a fájlrendszerben, vagy technikailag hibás

- hidden file                     <-  Titkos adatokat tartalmazó fájl, ".hidden" kiterjesztéssel
  - actual (aktuális)             <-  Olyan hidden fájl ami az aktuális adatokat tartalmazza, és módosítható
  - old (régi)                    <-  Régi hidden fájl, elévült adatokkal, és csak olvasható

- note (bejegyzés)                <-  Bejegyzés egy hidden fájlon belül
  - note name (behegyzés név)   
  - note text (bejegyzés szöveg)   

- baseDir (alapkönyvtár)          <-  Alapkönyvtár, minden adat gyökere. Program indítás attribútummal van megadva.

- ticketDir                       <-  {baseDir}/{ticket} 

- ticketOldDir                    <-  {baseDir}/{ticket}/old 

- saltHidden                      <-  Titkosításhoz használt salt érték. Minden hidden-rendszeren más lehet.
                                      Program indítási paraméter (app.salt), mely nem publikus.

- salt0                           <-  Titkosításhoz használt salt érték, mely fix 16 darab 0 értékű bájt, 
                                      így minden hidden-rendszeren ugyanaz.
                                      Akkor használjuk, ha hidden rendszerek között szeretnénk mozgatni a fájlokat.

--------------------------------------------------------------------------------------------------------------------------------
HFVC

- HFVC = Hidden File Version Conversion

- A HFVC meghatározza, hogy a program milyen verziójú hidden-fájlt képes beolvasni, és milyen verziójú fájlt ír mentéskor.
  Leírás formája:

    HFVC: r1,r2,... -> w3

  melyben "r"-rel vannak jelölve a beolvasható verziók, "w"-vel pedig a fájl írásakor készülő verzió. Példák:

    HFVC: 2,3 -> 3
    HFVC: 3 -> 3

- Egy hidden-fájl verziója a fájl 7. bájtja, ha a "hidden" 6 bájtjával kezdődik. 
  A 7. bájtot 0-255 értékként értelmezünk!
  Ha egy fájl nem a "hidden" 6 bájtjával kezdődik, akkor az nem hidden-fájl, nem határozható meg a verziója.  

--------------------------------------------------------------------------------------------------------------------------------